#include "midi.h"

/*
int main(void)
{
	char file_name[100] = { 0, };
//	cout << "Enter Music Name" << endl;
//	cin >> file_name;//After finish this source, active this line.
//	cin.ignore();//either.

	strncpy_s(file_name, "Wings Of Piano.mid", strlen("Wings Of Piano.mid"));

	MIDI(file_name);

	return 0;
}
*/

/*
void MIDI(char arg_file_name[])
{
	FILE *FD_midi, **FD_track_arr;
	char *file_name = NULL;
	char str[5] = {0, };
	int i, file_name_len, *queue_arr, tmp, j;
	uint64_t file_size, *delta_time_arr, *Full_delta_time_arr, min_delta_time;
	double* timer;

	MTRK_DATA *mtrk_data;

	//
	//시간을 계산하기 위한 변수들을 TIMER 구조체에 넣어버림. NOTE_GAME_PROJECT와 합칠 시 수정이 필요하다.
	//아마 삭제하지 않을까 싶음.
	//
	TIMER win_timer;

	//파일을 여는 과정
	{
		file_name_len = strlen(arg_file_name) + strlen("./") + 1;

		file_name = (char *)calloc(file_name_len, sizeof(char));

		strncpy_s(file_name, file_name_len, "./", sizeof("./") / sizeof(char));

		strncat_s(file_name, file_name_len, arg_file_name, strlen(arg_file_name));

		memset(&mthd_data, 0, sizeof(MTHD_DATA));

		fopen_s(&FD_midi, file_name, "rb");
	}

	if (NULL != FD_midi)
	{
		//system("mode con:cols=300 lines=30");
		file_size = GET_FILE_SIZE(FD_midi);

		//MTHD 부분
		{
			for (i = 0; i < 4; i++)
			{
				str[i] = fgetc(FD_midi);
			}
			if (strcmp(str, "MThd") == 0)
			{
				MTHD_FUNCTION(FD_midi);
			}
			else
			{
				printf("OPEN_MThd_ERROR!\n");
			}
		}

		//초당 틱수 설정
		{
			setting.SecondsPerTick = 500000.0f / 1000000.0f / float(mthd_data.ticks_per_quater_note);
		}

		//메모리 초기화
		{
			FD_track_arr = (FILE**)malloc(sizeof(FILE*)*(mthd_data.mtrk_cnt));//track의 개수만큼 메모리 할당.
			delta_time_arr = (uint64_t*)calloc(mthd_data.mtrk_cnt, sizeof(uint64_t));
			Full_delta_time_arr = (uint64_t*)calloc(mthd_data.mtrk_cnt, sizeof(uint64_t));
			mtrk_data = (MTRK_DATA*)calloc(mthd_data.mtrk_cnt, sizeof(MTRK_DATA));
			timer = (double*)calloc(mthd_data.mtrk_cnt, sizeof(double));
			queue_arr = (int*)calloc(mthd_data.mtrk_cnt, sizeof(int));
		}

		//MTRK시작부분 찾기
		for (i = 0; i < mthd_data.mtrk_cnt; i++)
		{
			FIND_NEXT_MTRK(FD_midi);
			fopen_s(&FD_track_arr[i], file_name, "rb");
			fseek(FD_track_arr[i], ftell(FD_midi), SEEK_SET);
		}

		//
		//MTRK_DATA_SIZE를 읽어야함!
		//DATA_SIZE를 읽어서 쓸일은 없겠지만 4byte만큼의 블럭을 읽어야 하므로 사용한다.
		//그 이유는 FF 2F 00을 가지고 TRACK의 끝을 판단하기 때문이다.
		//
		for (i = 0; i < mthd_data.mtrk_cnt; i++)
		{
			GET_DATA_SIZE(FD_track_arr[i]);
		}

		setting.left_mtrk_cnt = mthd_data.mtrk_cnt;
		
		for (i = 0; i < mthd_data.mtrk_cnt; i++)
		{
			queue_arr[i] = i;
		}


		//CPU의 주파수를 설정함. 이를 통해 정확한 시간을 측정할 예정.
		QueryPerformanceFrequency(&win_timer.Frequency);

		//노트를 플레이 하기 전 시간
		QueryPerformanceCounter(&win_timer.BeginTime);

		//DELTA_TIME을 먼저 확인해야함.
		for (i = 0; i < mthd_data.mtrk_cnt; i++)
		{
			//testing
			READ_DELTA_TIME(FD_track_arr[i], &(delta_time_arr[i]), &(Full_delta_time_arr[i]), &(timer[i]));
		}

		while (1)
		{
			//각 반복문당 걸린 시간을 체크하기 위해 타이머를 시작함.
			QueryPerformanceCounter(&win_timer.BeginNote);

			//모든 트랙을 읽었는지 확인하는 조건문
			if (setting.EndFlag == mthd_data.mtrk_cnt)
				break;

			//가장 낮은 delta_time을 가진 Track이 queue의 맨 앞에 오도록 설정.
			for (j = setting.EndFlag; j < mthd_data.mtrk_cnt - 1; j++)
			{
				for (i = setting.EndFlag; i < mthd_data.mtrk_cnt - j + setting.EndFlag - 1; i++)
				{
					if (delta_time_arr[queue_arr[i]] > delta_time_arr[queue_arr[i + 1]])
					{
						tmp = queue_arr[i];
						queue_arr[i] = queue_arr[i + 1];
						queue_arr[i + 1] = tmp;
					}
				}
			}

			//최소 delta_time을 찾음.
			min_delta_time = delta_time_arr[queue_arr[setting.EndFlag]];

			//delta_time만큼의 시간이 지났으므로 모든 track의 delta_time이 흐른것으로 파악, delta_time만큼 빼줌.
			for (i = setting.EndFlag; i < mthd_data.mtrk_cnt; i++)
			{
				delta_time_arr[queue_arr[i]] -= min_delta_time;
			}

			//
			//각 반복문당 걸린 시간을 체크하기 위한 타이머
			//delta_time만큼 시간이 지나지 않았을 경우 다시 타이머를 측정함으로써 적어도 delta_time만큼 시간이 흐르도록 함.
			//
			do {
				QueryPerformanceCounter(&win_timer.EndNote);
				win_timer.ElapsedNote.QuadPart = win_timer.EndNote.QuadPart - win_timer.BeginNote.QuadPart;

				win_timer.DuringNote = (double)win_timer.ElapsedNote.QuadPart / (double)win_timer.Frequency.QuadPart;
			} while (win_timer.DuringNote < (double)min_delta_time * setting.SecondsPerTick);

			//트랙을 읽는 함수
			MTRK_FUNCTION(FD_track_arr[queue_arr[setting.EndFlag]], &(delta_time_arr[queue_arr[setting.EndFlag]]), &(Full_delta_time_arr[queue_arr[setting.EndFlag]]), &(mtrk_data[queue_arr[setting.EndFlag]]), &(timer[queue_arr[setting.EndFlag]]));
		}

		//노트를 플레이 한 후 시간 계산
		QueryPerformanceCounter(&win_timer.EndTime);
		win_timer.Elapsed.QuadPart = win_timer.EndTime.QuadPart - win_timer.BeginTime.QuadPart;
		win_timer.Elapsed.QuadPart *= 1000000;
		win_timer.Duringtime = (double)win_timer.Elapsed.QuadPart / (double)win_timer.Frequency.QuadPart;

		//시간 출력
		printf("DuringTime : %lf\n", win_timer.Duringtime);

		//트렉번호당 끝난 위치와 해당 트렉당 걸린 시간.
		for (i = 0; i < mthd_data.mtrk_cnt; i++)
		{
			printf("\nTrackNumber : %d, Full_delta_time : %I64d, ", i + 1, Full_delta_time_arr[i]);
			printf("ftell : %d\n", ftell(FD_track_arr[i]));
			printf("\nTimer : %lf\n", timer[i]);
		}

		
		//끝을 확인하는 함수
		//마지막 트랙의 마지막 진행부분이 파일크기와 다를경우 파일을 끝까지 재생한 것이 아니기 때문에 에러로 판명한다.
		{
			if (file_size == ftell(FD_track_arr[mthd_data.mtrk_cnt-1]))
			{
				for (i = 0; i < mthd_data.mtrk_cnt; i++)
				{
					fclose(FD_track_arr[i]);
				}
				free(FD_track_arr);//track_arr 메모리 해제.
				free(delta_time_arr);
				free(Full_delta_time_arr);
				free(mtrk_data);
				free(timer);
				free(queue_arr);
				printf("PROGRAM_END\n");
			}
			else
			{
				for (i = 0; i < mthd_data.mtrk_cnt; i++)
				{
					fclose(FD_track_arr[i]);
				}
				free(FD_track_arr);//track_arr 메모리 해제.
				free(delta_time_arr);
				free(Full_delta_time_arr);
				free(mtrk_data);
				free(timer);
				free(queue_arr);
				printf("END OF ERROR!\n");
			}
		}
		
		
	}	
	else
		printf("FILE_OPEN_ERROR!\n");
	
}
*/

void MTHD_FUNCTION(FILE* FD_midi, MTHD_DATA* mthd_data)
{
	uint64_t size = GET_DATA_SIZE(FD_midi);

	if (size != 6)
	{
		printf("ERROR!\n");
	}

	mthd_data->format = fgetc(FD_midi);
	mthd_data->format = mthd_data->format << 8;
	mthd_data->format += fgetc(FD_midi);

	mthd_data->mtrk_cnt = fgetc(FD_midi);
	mthd_data->mtrk_cnt = mthd_data->mtrk_cnt << 8;
	mthd_data->mtrk_cnt += fgetc(FD_midi);

	mthd_data->ticks_per_quater_note = fgetc(FD_midi);
	if (mthd_data->ticks_per_quater_note < 0x80)
	{
		mthd_data->ticks_per_quater_note = mthd_data->ticks_per_quater_note << 8;
		mthd_data->ticks_per_quater_note += fgetc(FD_midi);
	}
	else
	{
		int A = mthd_data->ticks_per_quater_note;//1초에 돌아가는 SMTPE 프레임 수임.
		int B = fgetc(FD_midi);//SMTPE 프레임 한 개에 필요한 단위 시간의 수임. 
	}
}

void MTRK_FUNCTION(FILE* FD_midi, MTRK_DATA* mtrk_data, double* timer, char* blocks, MIDI_SETTING* midi_setting, MTHD_DATA* mthd_data)
{
	short f_ch = 0;
	//BPM은 곡이 실행되면서 여러번 바뀐다. 기본은 120. 노트와 음악의 싱크를 맞추기 위해서 해결해야 할 중요한 문제!

	uint64_t delta_time;

	//Start_loop
	while(0 >= *timer)
	{
		//printf("1");//DEBUG_LINE
		//0x80보다 같거나 클 경우 그대로
		READ_MIDI(FD_midi);

		if (f_ch >= 0x80)
		{
			mtrk_data->status = f_ch;
			mtrk_data->h_status = f_ch / 0x10;
			mtrk_data->l_status = f_ch % 0x10;

			short key;

			switch (mtrk_data->h_status){
			case 8://Note off
				READ_MIDI(FD_midi);//Note Number;
				//NOTE[f_ch%12] = 0;
				//blocks[f_ch % 12] = 0;
				switch (f_ch % 12)
				{
				case 0:
				case 1:
					blocks[0] = 0;
					break;
				case 2:
				case 3:
					blocks[1] = 0;
					break;
				case 4:
					blocks[2] = 0;
					break;
				case 5:
				case 6:
					blocks[3] = 0;
					break;
				case 7:
				case 8:
					blocks[4] = 0;
					break;
				case 9:
				case 10:
					blocks[5] = 0;
					break;
				case 11:
					blocks[6] = 0;
				}
				READ_MIDI(FD_midi);//Note Velocity;
				printf("Note_Velocity : %d\n", f_ch);//DEBUG_LILNE
				break;
			case 9://Note on
				READ_MIDI(FD_midi);//Note Number;
				key = f_ch;
				READ_MIDI(FD_midi);//Note Velocity;
				//NOTE[key%12] = (bool)f_ch;
				printf("Note_Velocity : %d\n", f_ch);//DEBUG_LILNE
				//blocks[key % 12] = (bool)f_ch;
				switch (key % 12)
				{
				case 0:
				case 1:
					blocks[0] = (bool)f_ch;
					break;
				case 2:
				case 3:
					blocks[1] = (bool)f_ch;
					break;
				case 4:
					blocks[2] = (bool)f_ch;
					break;
				case 5:
				case 6:
					blocks[3] = (bool)f_ch;
					break;
				case 7:
				case 8:
					blocks[4] = (bool)f_ch;
					break;
				case 9:
				case 10:
					blocks[5] = (bool)f_ch;
					break;
				case 11:
					blocks[6] = (bool)f_ch;
				}
				break;
			case 0xA://Polyphonic aftertouch
					 //신경쓸 필요 없는 부분(그냥 데이터만 받고 보내버리자)
				READ_MIDI(FD_midi);//Note Number;
				READ_MIDI(FD_midi);//Aftertouch pressure;
				break;
			case 0xB://Control/Mode change
					 //Need More Data
				READ_MIDI(FD_midi);//Controller number
				if (f_ch < 0x60 || f_ch == 0x7A)
				{
					READ_MIDI(FD_midi);//New value
				}
				break;
			case 0xC://Program change; f_ch = Program number
			case 0xD://Channel Aftertouch
				READ_MIDI(FD_midi);//Aftertouch pressure
				break;
			case 0xE://Pitch wheel range
				READ_MIDI(FD_midi);//Least significant byte
				READ_MIDI(FD_midi);//Most significant byte
				break;
			case 0xF://System
					 //0xF0, 0xF7, 0xFF를 제외하고는 실제 파일에서 보기 힘들다고함.(본적이 없다고함.)
				switch (mtrk_data->l_status)//이부분은 정확히 정리가 되지 않음! 다시 한번 살펴볼것!
				{
				case 0://System Exclusive
					   //F0 or F7으로 시작하여 F7으로 끝나는 데이터는 씹어버리면 그만
					do {
						READ_MIDI(FD_midi);
					} while (f_ch != 0xF7);
					break;
				case 1://Undefined
					break;
				case 2://Sys Song Position Pointer
					READ_MIDI(FD_midi);//LSB
					READ_MIDI(FD_midi);//MSB
					break;
				case 3://Sys Song Select
					READ_MIDI(FD_midi);//Song Number
					break;
				case 4://undefined
				case 5://undefined
				case 6://Sys Tune Request
					break;//Upon receiving a Tune Request, all analog synthesisers should tune their oscillators.
				case 7://Sys End of sysEx(EOX)
					   //F0 or F7으로 시작하여 F7으로 끝나는 데이터는 씹어버리면 그만
					do {
						READ_MIDI(FD_midi);
					} while (f_ch != 0xF7);
				case 8://Sys timing clock //Sent 24 times per quarter note when synchronisation is required.(Maybe... In this program, not needed.)
				case 9://Sys undefined
				case 0xA://Sys start //Start the current sequence playing.(This message will be followed with Timing Clocks).(Maybe... it's too...)
				case 0xB://Sys continue; //Continue at the point the sequence was Stopped.(Maybe... it's too...)
				case 0xC://Sys stop //Stop the current sequence.(Maybe... it's too...)
				case 0xD://Sys undefined
				case 0xE://Sys active sensing //Read structure file. too long...
					break;
				case 0xF://Sys reset
					READ_MIDI(FD_midi);
					switch (f_ch)
					{
					case 0:
						READ_MIDI(FD_midi);
						if (f_ch != 2)
						{
							printf("FF 00_Error!\n");
							exit(0);
						}
						READ_MIDI(FD_midi);//ss
						READ_MIDI(FD_midi);//ss
						break;
					case 1://text
					case 2://Copyright
					case 3://Sequence/Track Name
					case 4://Instrument Name
					case 5://Lyric
					case 6://Marker
					case 7://Cue Point
					case 8://Program Name
					case 9://Device name
					case 0x0A:
						mtrk_data->v_length = GET_V_LENGTH(FD_midi);//v_length
						for (int i = 0; i < mtrk_data->v_length; i++)
						{
							READ_MIDI(FD_midi);//text
						}
						break;
					case 0x20://MIDI Channel Prefix
						READ_MIDI(FD_midi);//must 01
						if (f_ch != 1)
						{
							printf("FF 20 ERROR!\n");
							exit(0);//Error
						}
						READ_MIDI(FD_midi);//cc
						break;
					case 0x2F://End Of Track
						READ_MIDI(FD_midi);
						if (f_ch == 0x00)
						{
							midi_setting->EndFlag += 1;
							midi_setting->left_mtrk_cnt -= 1;
							mtrk_data->delta_time = 0xFFFFFFFF;
							printf("\nEOF!-----------------------------\n");
							return;
							//break;
						}
						else
						{
							printf("FF 2F ERROR!\n");
							exit(0);
						}
						break;
					case 0x51://Set Tempo
						//----------------완료-------------------------------
						READ_MIDI(FD_midi);
						if (f_ch != 0x03)
						{
							printf("FF 51 ERROR!\n");
							exit(0);
						}
						else
						{
							uint64_t MicrosecondsPerQuarterNote = 0;
							for (int i = 0; i < 3; i++)
							{
								READ_MIDI(FD_midi);
								MicrosecondsPerQuarterNote = MicrosecondsPerQuarterNote << 8;
								MicrosecondsPerQuarterNote += f_ch;
							}
							//mtrk_data->BPM = TME * 60 / 1000000;
							midi_setting->BPM = (60000000.0f / MicrosecondsPerQuarterNote * midi_setting->TimeSignatureDenominator / 4) + 0.5;//반올림으로 만들기 위해 0.5를 추가함.

							//테스트를 시작할 부분
							midi_setting->SecondsPerTick = MicrosecondsPerQuarterNote / 1000000.0f / mthd_data->ticks_per_quater_note;

						}
						break;
					case 0x54://SMTPE Offset
						READ_MIDI(FD_midi);
						if (f_ch != 0x05)
						{
							printf("FF 54 ERROR!\n");
							exit(0);
						}
						else
						{
							//수정중
							{
								int time = 0;//time 은 seconds로 계산한다.
								READ_MIDI(FD_midi);//hh
								time += f_ch * 60 * 60;
								READ_MIDI(FD_midi);//mm
								time += f_ch * 60;
								READ_MIDI(FD_midi);//ss
								time += f_ch;
								//hour, minutes, seconds의 시간만큼 이후에 이 트렉이 시작되어야 한다.
							}
							READ_MIDI(FD_midi);//fr
							READ_MIDI(FD_midi);//ff

						}
						break;
					case 0x58://Time Signature//몇분의 몇박자인지와 메트로늄의 시간간격, 마지막은 4분음표를 표현하기위해 32분음표가 몇개 필요한지 이다.
						READ_MIDI(FD_midi);
						if (f_ch != 0x04)
						{
							printf("FF 58 ERROR!\n");
							exit(0);
						}
						else
						{
							midi_setting->TimeSignatureNumerator = READ_MIDI(FD_midi);//nn
							midi_setting->TimeSignatureDenominator = pow(2, READ_MIDI(FD_midi));//dd
							READ_MIDI(FD_midi);//cc
							READ_MIDI(FD_midi);//bb
						}
						break;
					case 0x59://Key Signature//조표를 나타내는 것으로 우리와는 무관하다!
						READ_MIDI(FD_midi);
						if (f_ch != 0x02)
						{
							printf("FF 59 ERROR!\n");
							exit(0);
						}
						else
						{
							READ_MIDI(FD_midi);//sf
							READ_MIDI(FD_midi);//mi
						}
						break;
					case 0x7F://Sequencer-Specific Meta-event
						mtrk_data->v_length = GET_V_LENGTH(FD_midi);
						for (int i = 0; i < mtrk_data->v_length; i++)
						{
							READ_MIDI(FD_midi);
						}
						break;
					}
				}
				break;
			default:
				break;
			}
		}
		//0x80보다 작을 경우 Data1으로 인식.
		else
		{
			//status관련은 이전 status사용.
			mtrk_data->data1 = f_ch;
			switch (mtrk_data->h_status) {
			case 8://Note off
				//mtrk_data->octave = (mtrk_data->data1 / 12) - 1;
				//mtrk_data->note_num = mtrk_data->data1 % 12;
				//NOTE[mtrk_data->data1 % 12] = 0;
				switch (mtrk_data->data1 % 12)
				{
				case 0:
				case 1:
					blocks[0] = 0;
					break;
				case 2:
				case 3:
					blocks[1] = 0;
					break;
				case 4:
					blocks[2] = 0;
					break;
				case 5:
				case 6:
					blocks[3] = 0;
					break;
				case 7:
				case 8:
					blocks[4] = 0;
					break;
				case 9:
				case 10:
					blocks[5] = 0;
					break;
				case 11:
					blocks[6] = 0;
				}
				//blocks[mtrk_data->data1 % 12] = 0;
				READ_MIDI(FD_midi);//Note Velocity;
				printf("Note_Velocity : %d\n", f_ch);//DEBUG_LILNE
				//mtrk_data->velocity = f_ch;
				break;
			case 9://Note on
				//mtrk_data->octave = (mtrk_data->data1 / 12) - 1;
				//mtrk_data->note_num = mtrk_data->data1 % 12;
				READ_MIDI(FD_midi);//Aftertouch pressure;
				//mtrk_data->velocity = f_ch;
				//NOTE[mtrk_data->data1 % 12] = (bool)f_ch;
				//blocks[mtrk_data->data1 % 12] = (bool)f_ch;
				switch (mtrk_data->data1 % 12)
				{
				case 0:
				case 1:
					blocks[0] = (bool)f_ch;
					break;
				case 2:
				case 3:
					blocks[1] = (bool)f_ch;
					break;
				case 4:
					blocks[2] = (bool)f_ch;
					break;
				case 5:
				case 6:
					blocks[3] = (bool)f_ch;
					break;
				case 7:
				case 8:
					blocks[4] = (bool)f_ch;
					break;
				case 9:
				case 10:
					blocks[5] = (bool)f_ch;
					break;
				case 11:
					blocks[6] = (bool)f_ch;
				}
				printf("Note_Velocity : %d\n", f_ch);//DEBUG_LILNE
				break;
			case 0xA://Polyphonic aftertouch
					 //신경쓸 필요 없는 부분(그냥 데이터만 받고 보내버리자)
				READ_MIDI(FD_midi);//Aftertouch pressure;
				break;
			case 0xB://Control/Mode change
					 //Need More Data
				if (f_ch < 0x60 || f_ch == 0x7A)
					READ_MIDI(FD_midi);//New value
				break;
			case 0xC://Program change;
			case 0xD://Channel Aftertouch
				break;
			case 0xE://Pitch wheel range
				READ_MIDI(FD_midi);//Most significant byte
				break;
			case 0xF://System
					 //0xF0, 0xF7, 0xFF를 제외하고는 실제 파일에서 보기 힘들다고함.(본적이 없다고함.)
				switch (mtrk_data->l_status)//이부분은 정확히 정리가 되지 않음! 다시 한번 살펴볼것!
				{
				case 0://System Exclusive
					   //F0 or F7으로 시작하여 F7으로 끝나는 데이터는 씹어버리면 그만
					while (f_ch != 0xF7)
					{
						READ_MIDI(FD_midi);
					}
					break;
				case 1://Undefined
					break;
				case 2://Sys Song Position Pointer
					//READ_MIDI(FD_midi);//LSB//data1==f_ch
					READ_MIDI(FD_midi);//MSB
					break;
				case 3://Sys Song Select
				case 4://undefined
				case 5://undefined
				case 6://Sys Tune Request
					break;//Upon receiving a Tune Request, all analog synthesisers should tune their oscillators.
				case 7://Sys End of sysEx(EOX)
					   //F0 or F7으로 시작하여 F7으로 끝나는 데이터는 씹어버리면 그만
					while (f_ch != 0xF7);
					{
						READ_MIDI(FD_midi);
					}
				case 8://Sys timing clock //Sent 24 times per quarter note when synchronisation is required.(Maybe... In this program, not needed.)
				case 9://Sys undefined
				case 0xA://Sys start //Start the current sequence playing.(This message will be followed with Timing Clocks).(Maybe... it's too...)
				case 0xB://Sys continue //Continue at the point the sequence was Stopped.(Maybe... it's too...)
				case 0xC://Sys stop //Stop the current sequence.(Maybe... it's too...)
				case 0xD://Sys undefined
				case 0xE://Sys active sensing
					break;//Read structure file. too long...
				case 0xF://Sys reset
					switch (f_ch)
					{
					case 0:
						READ_MIDI(FD_midi);
						if (f_ch != 2)
						{
							printf("FF 00 Error!\n");
							exit(0);
						}
						READ_MIDI(FD_midi);//ss
						READ_MIDI(FD_midi);//ss
						break;
					case 1://text
					case 2://Copyright
					case 3://Sequence/Track Name
					case 4://Instrument Name
					case 5://Lyric
					case 6://Marker
					case 7://Cue Point
					case 8://Program Name
					case 9://Device name
					case 0x0A:
						mtrk_data->v_length = GET_V_LENGTH(FD_midi);//v_length
						for (int i = 0; i < mtrk_data->v_length; i++)
						{
							READ_MIDI(FD_midi);//text
						}
						break;
					case 0x20://MIDI Channel Prefix
						READ_MIDI(FD_midi);//must 01
						if (f_ch != 1)
						{
							printf("FF 20 ERROR!\n");
							exit(0);//Error
						}
						READ_MIDI(FD_midi);//cc
						break;
					case 0x2F://End Of Track
						READ_MIDI(FD_midi);
						if (f_ch == 0x00)
						{
							midi_setting->EndFlag += 1;
							midi_setting->left_mtrk_cnt -= 1;
							mtrk_data->delta_time = 0xFFFFFFFF;
							printf("\nEOF------------------\n");
							return;
						}
						else
						{
							printf("FF 2F ERROR\n");
							exit(0);
						}
						break;
					case 0x51://Set Tempo
						//----------------완료-------------------------------
						READ_MIDI(FD_midi);
						if (f_ch != 0x03)
						{
							printf("FF 51 ERROR!\n");
							exit(0);
						}
						else
						{
							uint64_t MicrosecondsPerQuarterNote = 0;
							for (int i = 0; i < 3; i++)
							{
								READ_MIDI(FD_midi);
								MicrosecondsPerQuarterNote = MicrosecondsPerQuarterNote << 8;
								MicrosecondsPerQuarterNote += f_ch;
							}
							//mtrk_data->BPM = TME * 60 / 1000000;
							midi_setting->BPM = (60000000.0f / MicrosecondsPerQuarterNote * midi_setting->TimeSignatureDenominator / 4) + 0.5;//반올림으로 만들기 위해 0.5를 추가함.

							//테스트를 시작할 부분
							midi_setting->SecondsPerTick = MicrosecondsPerQuarterNote / 1000000.0f / mthd_data->ticks_per_quater_note;

						}
						break;
					case 0x54://SMTPE Offset
						READ_MIDI(FD_midi);
						if (f_ch != 0x05)
						{
							printf("FF 54 ERROR!\n");
							exit(0);
						}
						else
						{
							//수정중
							{
								int time = 0;//time 은 seconds로 계산한다.
								READ_MIDI(FD_midi);//hh
								time += f_ch * 60 * 60;
								READ_MIDI(FD_midi);//mm
								time += f_ch * 60;
								READ_MIDI(FD_midi);//ss
								time += f_ch;
								//hour, minutes, seconds의 시간만큼 이후에 이 트렉이 시작되어야 한다.
							}
							READ_MIDI(FD_midi);//fr
							READ_MIDI(FD_midi);//ff

						}
						break;
					case 0x58://Time Signature
						READ_MIDI(FD_midi);
						if (f_ch != 0x04)
						{
							printf("FF 58 ERROR!\n");
							exit(0);
						}
						else
						{
							midi_setting->TimeSignatureNumerator = READ_MIDI(FD_midi);//nn
							midi_setting->TimeSignatureDenominator = pow(2, READ_MIDI(FD_midi));//dd
							READ_MIDI(FD_midi);//cc
							READ_MIDI(FD_midi);//bb
						}
						break;
					case 0x59://Key Signature
						READ_MIDI(FD_midi);
						if (f_ch != 0x02)
						{
							printf("FF 59 ERROR!\n");
							exit(0);
						}
						else
						{
							READ_MIDI(FD_midi);//sf
							READ_MIDI(FD_midi);//mi
						}
						break;
					case 0x7F://Sequencer-Specific Meta-event
						mtrk_data->v_length = GET_V_LENGTH(FD_midi);
						for (int i = 0; i < mtrk_data->v_length; i++)
						{
							READ_MIDI(FD_midi);
						}
						break;
					}
				}
				break;
			default:
				break;
			}
		}

		//READ delta_time
		delta_time = 0;
		while (1)
		{
			delta_time = delta_time << 7;
			READ_MIDI(FD_midi);
			if (f_ch < 0x80)
			{
				delta_time += f_ch;
				break;
			}
			delta_time += f_ch - 0x80;
		}
		*timer += midi_setting->SecondsPerTick*(delta_time);
		//*Full_delta_time += *delta_time;
		//END READ delta_time
	}
	//printf("\n");//DEBUG_LINE
}

uint64_t GET_DATA_SIZE(FILE* FD_midi)
{
	uint64_t size = 0;
	char i;
	short data_size_tmp = 0;
	for(i = 0; i < 4; i++)
	{
		data_size_tmp = fgetc(FD_midi);
		size = size << 8;
		size += data_size_tmp;
	}
	return size;
}

uint64_t GET_FILE_SIZE(FILE* FD_midi)
{
	uint64_t file_size;
	//FIND_FILE_SIZE_START
	fseek(FD_midi, 0, SEEK_END);
	file_size = ftell(FD_midi);
	fseek(FD_midi, 0, SEEK_SET);

	return file_size;
}

uint64_t GET_V_LENGTH(FILE* FD_midi)
{
	uint64_t v_length = 0;
	short f_ch;
	while (1)
	{
		READ_MIDI(FD_midi);
		v_length = v_length << 7;
		if (f_ch < 0x80)
		{
			v_length += f_ch;
			break;
		}
		v_length += f_ch - 0x80;
	}
	return v_length;
}

void FIND_NEXT_MTRK(FILE* FD_midi)
{
	char str[5] = { 0, }, i;

	while (strcmp(str, "MTrk") != 0)
	{
		for (i = 0; i < 3; i++)
		{
			str[i] = str[i + 1];
		}
		str[3] = fgetc(FD_midi);
	}
}

void READ_DELTA_TIME(FILE* FD_midi, double* timer, MIDI_SETTING* midi_setting)
{
	short f_ch = 0;
	uint64_t delta_time = 0;
	while (1)
	{
		delta_time = delta_time << 7;
		READ_MIDI(FD_midi);
		if (f_ch < 0x80)
		{
			delta_time += f_ch;
			break;
		}
		delta_time += f_ch - 0x80;
	}
	*timer += midi_setting->SecondsPerTick*(delta_time);
	//*Full_delta_time += delta_time;
}

void READ_MUSIC_TIME(FILE* FD_midi, MTRK_DATA* mtrk_data, double* timer,  MIDI_SETTING* midi_setting, MTHD_DATA* mthd_data)
{
	short f_ch = 0;
	//BPM은 곡이 실행되면서 여러번 바뀐다. 기본은 120. 노트와 음악의 싱크를 맞추기 위해서 해결해야 할 중요한 문제!

	uint64_t delta_time;

	//Start_loop
	while (0 >= *timer)
	{
		//printf("1");//DEBUG_LINE
		//0x80보다 같거나 클 경우 그대로
		READ_MIDI(FD_midi);

		if (f_ch >= 0x80)
		{
			mtrk_data->status = f_ch;
			mtrk_data->h_status = f_ch / 0x10;
			mtrk_data->l_status = f_ch % 0x10;

			switch (mtrk_data->h_status) {
			case 8://Note off
				READ_MIDI(FD_midi);//Note Number;
				READ_MIDI(FD_midi);//Note Velocity;
				break;
			case 9://Note on
				READ_MIDI(FD_midi);//Note Number;
				READ_MIDI(FD_midi);//Note Velocity;
				break;
			case 0xA://Polyphonic aftertouch
					 //신경쓸 필요 없는 부분(그냥 데이터만 받고 보내버리자)
				READ_MIDI(FD_midi);//Note Number;
				READ_MIDI(FD_midi);//Aftertouch pressure;
				break;
			case 0xB://Control/Mode change
					 //Need More Data
				READ_MIDI(FD_midi);//Controller number
				if (f_ch < 0x60 || f_ch == 0x7A)
				{
					READ_MIDI(FD_midi);//New value
				}
				break;
			case 0xC://Program change; f_ch = Program number
			case 0xD://Channel Aftertouch
				READ_MIDI(FD_midi);//Aftertouch pressure
				break;
			case 0xE://Pitch wheel range
				READ_MIDI(FD_midi);//Least significant byte
				READ_MIDI(FD_midi);//Most significant byte
				break;
			case 0xF://System
					 //0xF0, 0xF7, 0xFF를 제외하고는 실제 파일에서 보기 힘들다고함.(본적이 없다고함.)
				switch (mtrk_data->l_status)//이부분은 정확히 정리가 되지 않음! 다시 한번 살펴볼것!
				{
				case 0://System Exclusive
					   //F0 or F7으로 시작하여 F7으로 끝나는 데이터는 씹어버리면 그만
					do {
						READ_MIDI(FD_midi);
					} while (f_ch != 0xF7);
					break;
				case 1://Undefined
					break;
				case 2://Sys Song Position Pointer
					READ_MIDI(FD_midi);//LSB
					READ_MIDI(FD_midi);//MSB
					break;
				case 3://Sys Song Select
					READ_MIDI(FD_midi);//Song Number
					break;
				case 4://undefined
				case 5://undefined
				case 6://Sys Tune Request
					break;//Upon receiving a Tune Request, all analog synthesisers should tune their oscillators.
				case 7://Sys End of sysEx(EOX)
					   //F0 or F7으로 시작하여 F7으로 끝나는 데이터는 씹어버리면 그만
					do {
						READ_MIDI(FD_midi);
					} while (f_ch != 0xF7);
				case 8://Sys timing clock //Sent 24 times per quarter note when synchronisation is required.(Maybe... In this program, not needed.)
				case 9://Sys undefined
				case 0xA://Sys start //Start the current sequence playing.(This message will be followed with Timing Clocks).(Maybe... it's too...)
				case 0xB://Sys continue; //Continue at the point the sequence was Stopped.(Maybe... it's too...)
				case 0xC://Sys stop //Stop the current sequence.(Maybe... it's too...)
				case 0xD://Sys undefined
				case 0xE://Sys active sensing //Read structure file. too long...
					break;
				case 0xF://Sys reset
					READ_MIDI(FD_midi);
					switch (f_ch)
					{
					case 0:
						READ_MIDI(FD_midi);
						if (f_ch != 2)
						{
							printf("FF 00_Error!\n");
							exit(0);
						}
						READ_MIDI(FD_midi);//ss
						READ_MIDI(FD_midi);//ss
						break;
					case 1://text
					case 2://Copyright
					case 3://Sequence/Track Name
					case 4://Instrument Name
					case 5://Lyric
					case 6://Marker
					case 7://Cue Point
					case 8://Program Name
					case 9://Device name
					case 0x0A:
						mtrk_data->v_length = GET_V_LENGTH(FD_midi);//v_length
						for (int i = 0; i < mtrk_data->v_length; i++)
						{
							READ_MIDI(FD_midi);//text
						}
						break;
					case 0x20://MIDI Channel Prefix
						READ_MIDI(FD_midi);//must 01
						if (f_ch != 1)
						{
							printf("FF 20 ERROR!\n");
							exit(0);//Error
						}
						READ_MIDI(FD_midi);//cc
						break;
					case 0x2F://End Of Track
						READ_MIDI(FD_midi);
						if (f_ch == 0x00)
						{
							midi_setting->EndFlag += 1;
							midi_setting->left_mtrk_cnt -= 1;
							mtrk_data->delta_time = 0xFFFFFFFF;
							printf("\nEOF!-----------------------------\n");
							return;
							//break;
						}
						else
						{
							printf("FF 2F ERROR!\n");
							exit(0);
						}
						break;
					case 0x51://Set Tempo
							  //----------------완료-------------------------------
						READ_MIDI(FD_midi);
						if (f_ch != 0x03)
						{
							printf("FF 51 ERROR!\n");
							exit(0);
						}
						else
						{
							uint64_t MicrosecondsPerQuarterNote = 0;
							for (int i = 0; i < 3; i++)
							{
								READ_MIDI(FD_midi);
								MicrosecondsPerQuarterNote = MicrosecondsPerQuarterNote << 8;
								MicrosecondsPerQuarterNote += f_ch;
							}
							//mtrk_data->BPM = TME * 60 / 1000000;
							midi_setting->BPM = (60000000.0f / MicrosecondsPerQuarterNote * midi_setting->TimeSignatureDenominator / 4) + 0.5;//반올림으로 만들기 위해 0.5를 추가함.

																																			  //테스트를 시작할 부분
							midi_setting->SecondsPerTick = MicrosecondsPerQuarterNote / 1000000.0f / mthd_data->ticks_per_quater_note;

						}
						break;
					case 0x54://SMTPE Offset
						READ_MIDI(FD_midi);
						if (f_ch != 0x05)
						{
							printf("FF 54 ERROR!\n");
							exit(0);
						}
						else
						{
							//수정중
							{
								int time = 0;//time 은 seconds로 계산한다.
								READ_MIDI(FD_midi);//hh
								time += f_ch * 60 * 60;
								READ_MIDI(FD_midi);//mm
								time += f_ch * 60;
								READ_MIDI(FD_midi);//ss
								time += f_ch;
								//hour, minutes, seconds의 시간만큼 이후에 이 트렉이 시작되어야 한다.
							}
							READ_MIDI(FD_midi);//fr
							READ_MIDI(FD_midi);//ff

						}
						break;
					case 0x58://Time Signature//몇분의 몇박자인지와 메트로늄의 시간간격, 마지막은 4분음표를 표현하기위해 32분음표가 몇개 필요한지 이다.
						READ_MIDI(FD_midi);
						if (f_ch != 0x04)
						{
							printf("FF 58 ERROR!\n");
							exit(0);
						}
						else
						{
							midi_setting->TimeSignatureNumerator = READ_MIDI(FD_midi);//nn
							midi_setting->TimeSignatureDenominator = pow(2, READ_MIDI(FD_midi));//dd
							READ_MIDI(FD_midi);//cc
							READ_MIDI(FD_midi);//bb
						}
						break;
					case 0x59://Key Signature//조표를 나타내는 것으로 우리와는 무관하다!
						READ_MIDI(FD_midi);
						if (f_ch != 0x02)
						{
							printf("FF 59 ERROR!\n");
							exit(0);
						}
						else
						{
							READ_MIDI(FD_midi);//sf
							READ_MIDI(FD_midi);//mi
						}
						break;
					case 0x7F://Sequencer-Specific Meta-event
						mtrk_data->v_length = GET_V_LENGTH(FD_midi);
						for (int i = 0; i < mtrk_data->v_length; i++)
						{
							READ_MIDI(FD_midi);
						}
						break;
					}
				}
				break;
			default:
				break;
			}
		}
		//0x80보다 작을 경우 Data1으로 인식.
		else
		{
			//status관련은 이전 status사용.
			mtrk_data->data1 = f_ch;
			switch (mtrk_data->h_status) {
			case 8://Note off
				READ_MIDI(FD_midi);//Note Velocity;
				break;
			case 9://Note on
				READ_MIDI(FD_midi);//Aftertouch pressure;
				break;
			case 0xA://Polyphonic aftertouch
					 //신경쓸 필요 없는 부분(그냥 데이터만 받고 보내버리자)
				READ_MIDI(FD_midi);//Aftertouch pressure;
				break;
			case 0xB://Control/Mode change
					 //Need More Data
				if (f_ch < 0x60 || f_ch == 0x7A)
					READ_MIDI(FD_midi);//New value
				break;
			case 0xC://Program change;
			case 0xD://Channel Aftertouch
				break;
			case 0xE://Pitch wheel range
				READ_MIDI(FD_midi);//Most significant byte
				break;
			case 0xF://System
					 //0xF0, 0xF7, 0xFF를 제외하고는 실제 파일에서 보기 힘들다고함.(본적이 없다고함.)
				switch (mtrk_data->l_status)//이부분은 정확히 정리가 되지 않음! 다시 한번 살펴볼것!
				{
				case 0://System Exclusive
					   //F0 or F7으로 시작하여 F7으로 끝나는 데이터는 씹어버리면 그만
					while (f_ch != 0xF7)
					{
						READ_MIDI(FD_midi);
					}
					break;
				case 1://Undefined
					break;
				case 2://Sys Song Position Pointer
					   //READ_MIDI(FD_midi);//LSB//data1==f_ch
					READ_MIDI(FD_midi);//MSB
					break;
				case 3://Sys Song Select
				case 4://undefined
				case 5://undefined
				case 6://Sys Tune Request
					break;//Upon receiving a Tune Request, all analog synthesisers should tune their oscillators.
				case 7://Sys End of sysEx(EOX)
					   //F0 or F7으로 시작하여 F7으로 끝나는 데이터는 씹어버리면 그만
					while (f_ch != 0xF7);
					{
						READ_MIDI(FD_midi);
					}
				case 8://Sys timing clock //Sent 24 times per quarter note when synchronisation is required.(Maybe... In this program, not needed.)
				case 9://Sys undefined
				case 0xA://Sys start //Start the current sequence playing.(This message will be followed with Timing Clocks).(Maybe... it's too...)
				case 0xB://Sys continue //Continue at the point the sequence was Stopped.(Maybe... it's too...)
				case 0xC://Sys stop //Stop the current sequence.(Maybe... it's too...)
				case 0xD://Sys undefined
				case 0xE://Sys active sensing
					break;//Read structure file. too long...
				case 0xF://Sys reset
					switch (f_ch)
					{
					case 0:
						READ_MIDI(FD_midi);
						if (f_ch != 2)
						{
							printf("FF 00 Error!\n");
							exit(0);
						}
						READ_MIDI(FD_midi);//ss
						READ_MIDI(FD_midi);//ss
						break;
					case 1://text
					case 2://Copyright
					case 3://Sequence/Track Name
					case 4://Instrument Name
					case 5://Lyric
					case 6://Marker
					case 7://Cue Point
					case 8://Program Name
					case 9://Device name
					case 0x0A:
						mtrk_data->v_length = GET_V_LENGTH(FD_midi);//v_length
						for (int i = 0; i < mtrk_data->v_length; i++)
						{
							READ_MIDI(FD_midi);//text
						}
						break;
					case 0x20://MIDI Channel Prefix
						READ_MIDI(FD_midi);//must 01
						if (f_ch != 1)
						{
							printf("FF 20 ERROR!\n");
							exit(0);//Error
						}
						READ_MIDI(FD_midi);//cc
						break;
					case 0x2F://End Of Track
						READ_MIDI(FD_midi);
						if (f_ch == 0x00)
						{
							midi_setting->EndFlag += 1;
							midi_setting->left_mtrk_cnt -= 1;
							mtrk_data->delta_time = 0xFFFFFFFF;
							printf("\nEOF------------------\n");
							return;
						}
						else
						{
							printf("FF 2F ERROR\n");
							exit(0);
						}
						break;
					case 0x51://Set Tempo
							  //----------------완료-------------------------------
						READ_MIDI(FD_midi);
						if (f_ch != 0x03)
						{
							printf("FF 51 ERROR!\n");
							exit(0);
						}
						else
						{
							uint64_t MicrosecondsPerQuarterNote = 0;
							for (int i = 0; i < 3; i++)
							{
								READ_MIDI(FD_midi);
								MicrosecondsPerQuarterNote = MicrosecondsPerQuarterNote << 8;
								MicrosecondsPerQuarterNote += f_ch;
							}
							//mtrk_data->BPM = TME * 60 / 1000000;
							midi_setting->BPM = (60000000.0f / MicrosecondsPerQuarterNote * midi_setting->TimeSignatureDenominator / 4) + 0.5;//반올림으로 만들기 위해 0.5를 추가함.

																																			  //테스트를 시작할 부분
							midi_setting->SecondsPerTick = MicrosecondsPerQuarterNote / 1000000.0f / mthd_data->ticks_per_quater_note;

						}
						break;
					case 0x54://SMTPE Offset
						READ_MIDI(FD_midi);
						if (f_ch != 0x05)
						{
							printf("FF 54 ERROR!\n");
							exit(0);
						}
						else
						{
							//수정중
							{
								int time = 0;//time 은 seconds로 계산한다.
								READ_MIDI(FD_midi);//hh
								time += f_ch * 60 * 60;
								READ_MIDI(FD_midi);//mm
								time += f_ch * 60;
								READ_MIDI(FD_midi);//ss
								time += f_ch;
								//hour, minutes, seconds의 시간만큼 이후에 이 트렉이 시작되어야 한다.
							}
							READ_MIDI(FD_midi);//fr
							READ_MIDI(FD_midi);//ff

						}
						break;
					case 0x58://Time Signature
						READ_MIDI(FD_midi);
						if (f_ch != 0x04)
						{
							printf("FF 58 ERROR!\n");
							exit(0);
						}
						else
						{
							midi_setting->TimeSignatureNumerator = READ_MIDI(FD_midi);//nn
							midi_setting->TimeSignatureDenominator = pow(2, READ_MIDI(FD_midi));//dd
							READ_MIDI(FD_midi);//cc
							READ_MIDI(FD_midi);//bb
						}
						break;
					case 0x59://Key Signature
						READ_MIDI(FD_midi);
						if (f_ch != 0x02)
						{
							printf("FF 59 ERROR!\n");
							exit(0);
						}
						else
						{
							READ_MIDI(FD_midi);//sf
							READ_MIDI(FD_midi);//mi
						}
						break;
					case 0x7F://Sequencer-Specific Meta-event
						mtrk_data->v_length = GET_V_LENGTH(FD_midi);
						for (int i = 0; i < mtrk_data->v_length; i++)
						{
							READ_MIDI(FD_midi);
						}
						break;
					}
				}
				break;
			default:
				break;
			}
		}

		//READ delta_time
		delta_time = 0;
		while (1)
		{
			delta_time = delta_time << 7;
			READ_MIDI(FD_midi);
			if (f_ch < 0x80)
			{
				delta_time += f_ch;
				break;
			}
			delta_time += f_ch - 0x80;
		}
		*timer += midi_setting->SecondsPerTick*(delta_time);
		//*Full_delta_time += *delta_time;
		//END READ delta_time
	}
	//printf("\n");//DEBUG_LINE
}

void CLEAR_MEMORY_MIDI_SETTING(MIDI_SETTING *midi_setting)
{
	memset(midi_setting, 0, sizeof(MIDI_SETTING));
	midi_setting->BPM = 120;
	midi_setting->TimeSignatureNumerator = 4;
	midi_setting->TimeSignatureDenominator = 4;
	midi_setting->EndFlag = 0;
}