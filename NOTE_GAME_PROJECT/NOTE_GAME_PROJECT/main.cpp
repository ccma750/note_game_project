#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <allegro5/allegro5.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_native_dialog.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

#include <sys/types.h>
#include "dirent.h"

#include "midi.h"

#define AL_DEBUG_LINE al_show_native_message_box(allegro_display, "Error", "Error", "AL_DEBUG_LINE!", NULL, ALLEGRO_MESSAGEBOX_ERROR);

const float FPS = 200;
const int NPS = 10;//NPS = Note Per Second
const int note_size_x = 70;
const int note_size_y = 20;
const int blocks_x = 7;
const int blocks_y = 60;
const int blocks_y_screen = 40;
const int SCREEN_W = 510;
const int SW_P_100 = SCREEN_W / 100;//SCREEN_W per 100
const int SCREEN_H = blocks_y_screen * note_size_y + 80;//상단 50px, 하단 30px
//const int SCREEN_H = 600;
const int bottom_line = blocks_y_screen * note_size_y + 50;

enum MYKEYS{KEY_Q, KEY_W, KEY_E, KEY_R, KEY_T, KEY_Y, KEY_U};
//KEY_I, KEY_O, KEY_P, KEY_OPENBRACE, KEY_CLOSEBRACE의 경우 12키에서 7키로 줄임에 따라 없앰.

void CAT_FILE_STR_BY_NOTE_COLOR(char str[], int x, int y);

int main()
{
	//아래 ALLEGRO 변수들은 프로그램이 끝날때까지 변하지 않는 변수.
	//한번 초기화 하고 프로그램이 끝나기전 한번 free해주면 그만.
	ALLEGRO_DISPLAY *allegro_display = NULL;
	ALLEGRO_EVENT_QUEUE *allegro_event_queue = NULL;
	ALLEGRO_BITMAP	*note_bitmap_array[blocks_x][4] = { NULL, };//0:normal, 1:long_note_up, 2:long_note_center, 3:long_note_down
	ALLEGRO_BITMAP	*press_key = NULL;
	ALLEGRO_BITMAP *line_bitmap = NULL;
	ALLEGRO_BITMAP *timer_bitmap = NULL;
	ALLEGRO_BITMAP *timer_airplane = NULL;
	ALLEGRO_FONT *score_font = NULL;

	//게임이 시작될 때마다 0으로 초기화 해야 한다.
	//al_set_timer_count(allegro_timer, 0);
	ALLEGRO_TIMER *allegro_timer = NULL;

	//allegro_timer로부터 count를 저장함.
	//하단에서 al_get_timer_count를 계속해서 호출할 경우 연산 낭비일거같아서 변수 하나를 선언해버림.
	uint64_t allegro_timer_count = 0;

	static int hit_line = FPS / NPS / 2;//hit_score를 정하는데 중점이 되는 부분이다.

	int i, j;//반복변수로 사용.
	int score = 0;
	int hit_score;//노트를 히트시켰을 시 점수. FPS/NPS의 관계로 이루어진다. LINE에서 멀어질수록 적은 점수를 부여할 예정.
	char delta;
	char blocks[blocks_y][blocks_x] = { false, };//진짜 노트들이 들어갈 자리이다.
	char garbage_blocks[blocks_y][blocks_x];//메인 트렉을 제외한 노트들이 들어갈 자리이다.
	char(**track_blocks)[blocks_x];//malloc으로 메모리를 할당해준 후 메인트렉은 blocks[][]를, 나머지는 garbage_blocks로 보낼 예정이다.
	bool redraw = true;
	bool key[blocks_x] = { 0, };
	bool finish_game = false;
	char score_str[10] = { 0, };

	bool end_program = false;
	bool goto_menu = false;
	bool read_file_name = true;
	double timestamp_esc = -1;
	double timestamp_enter = -1;

	//MIDI관련 변수들
	FILE *FD_midi;
	char *midi_file_name = NULL;//calloc으로 메모리 초기화.
	int midi_file_name_len;
	char midi_chunk_type[5] = { 0, };

	MIDI_SETTING midi_setting;
	MTHD_DATA mthd_data;

	FILE **FD_track_arr;
	MTRK_DATA *mtrk_data;
	double *midi_timer;
	int music_ending_time = -1;//마지막 내려오는 노트와 음악의 끝을 맞추기 위해 설정하는 변수, -1을 설정한 이유는 0일 경우에 노트가 완전히 끝난다고 판별하기 때문이다.
	int *midi_queue_arr;
	int tmp_midi_queue;

	//아래 세 변수는 해당 음악의 총 길이를 저장하는데 필요한 변수.
	double *track_timer;//각 트랙들의 시간을 저장함.
	double midi_timer_end;//해당 midi file의 끝나는 시간.(단위 : sec)
	uint64_t midi_timer_end_allegro_time;//midi_timer_end변수의 시간을 Allegro의 frame으로 나타낸것.

	//FMOD관련 변수들
	FMOD::System*	g_pSystem;
	FMOD::Sound*	g_pSound[SD_END];
	FMOD::Channel*	g_pChannel = 0;

	//MP3 관련 변수
	char *mp3_file_name;
	int mp3_file_name_len;

	//이경훈 파트
	int title_index = 0; // 곡순서 저장
	char track_num_index = 0; // 트랙번호 배열 index 
	char track_num_str[10][3];//
	char music_number = 0; // 음악 개수
	char *picture_name;
	char** title;
	//TESTLINE
	int track_ = 5; //트랙 갯수 임시 변수

					//file directory variable
	DIR *dp;
	struct dirent *dir;
	uint64_t max_file_strlen = 0;

	//key input variable
	bool up = false; // 방향키 위키 아래키 눌렸는지 구분
	bool down = false;
	bool right = false;
	bool left = false;

	//draw control variable
	bool done = false;
	bool draw = true;

	//Allegro variable
	ALLEGRO_EVENT event;
	ALLEGRO_FONT *font = NULL;
	ALLEGRO_BITMAP *title_bar = NULL;
	ALLEGRO_BITMAP *title_Upward = NULL;
	ALLEGRO_BITMAP *title_Downward = NULL;
	ALLEGRO_BITMAP *track_Rightward = NULL;
	ALLEGRO_BITMAP *track_Leftward = NULL;
	ALLEGRO_BITMAP *album_picture = NULL;//앨범 사진
	ALLEGRO_BITMAP *start_button = NULL;
	ALLEGRO_BITMAP *track_bar = NULL;
	ALLEGRO_EVENT_QUEUE *event_queue = NULL;

	//Allegro 초기화 부분
	//프로그램이 실행되고 단! 한번만 실행!
	//프로그램이 끝날때 단! 한번만 free!
	{
		//init 부분
		if (!al_init())
		{
			al_show_native_message_box(allegro_display, "Error", "Error", "Failed to initialize allegro", NULL, ALLEGRO_MESSAGEBOX_ERROR);
			return -1;
		}

		if (!al_init_image_addon())
		{
			al_show_native_message_box(allegro_display, "Error", "Error", "Failed to initialize al_init_image_addon!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
		}

		if (!al_install_keyboard())
		{
			al_show_native_message_box(allegro_display, "Error", "Error", "Failed to initialize the keyboard!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
		}

		if (!al_install_mouse())
		{
			al_show_native_message_box(allegro_display, "Error", "Error", "Failed to initialize the mouse!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
		}

		if (!al_init_font_addon())
		{
			al_show_native_message_box(allegro_display, "Error", "Error", "Failed to initialize the font!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
		}

		if (!al_init_ttf_addon())
		{
			al_show_native_message_box(allegro_display, "Error", "Error", "Failed to initialize the true type font!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
		}

		//init 완료

		allegro_display = al_create_display(SCREEN_W, SCREEN_H);

		if (!allegro_display)
		{
			al_show_native_message_box(allegro_display, "Error", "Error", "Failed to create display", NULL, ALLEGRO_MESSAGEBOX_ERROR);
			return -1;
		}

		allegro_timer = al_create_timer(1.0 / FPS);

		if (!allegro_timer)
		{
			al_show_native_message_box(allegro_display, "Error", "Error", "Failed to create timer!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
			al_destroy_display(allegro_display);
			return -1;
		}

		{
			char file_name[50];
			bool error = false;

			for (i = 0; i < blocks_x * 4; i++)
			{

				CAT_FILE_STR_BY_NOTE_COLOR(file_name, (int)(i / 4), i % 4);
				note_bitmap_array[i / 4][i % 4] = al_load_bitmap(file_name);

				if (!note_bitmap_array[i / 4][i % 4])
				{
					error = true;
					break;
				}
			}
			if (error)
			{
				char error_str[100] = "Failed to load ";
				strcat(error_str, file_name);
				al_show_native_message_box(allegro_display, "Error", "Error", error_str, NULL, ALLEGRO_MESSAGEBOX_ERROR);
				{
					al_destroy_display(allegro_display);
					al_destroy_timer(allegro_timer);
				}
				for (j = 0; j < i; j++)
				{
					al_destroy_bitmap(note_bitmap_array[j / 4][j % 4]);
				}
				return -1;
			}
		}

		press_key = al_load_bitmap("Press_Key.png");

		if (!press_key)
		{
			al_show_native_message_box(allegro_display, "Error", "Error", "Failed to load Press_Key.png!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
			al_destroy_display(allegro_display);
			al_destroy_timer(allegro_timer);
			for (i = 0; i < blocks_x; i++)
			{
				for (j = 0; j < 4; j++)
				{
					al_destroy_bitmap(note_bitmap_array[i][j]);
				}
			}
			return -1;
		}

		line_bitmap = al_create_bitmap(SCREEN_W, 1);
		if (!line_bitmap)
		{
			al_show_native_message_box(allegro_display, "Error", "Error", "Failed to load line!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
			al_destroy_display(allegro_display);
			al_destroy_timer(allegro_timer);
			for (i = 0; i < blocks_x; i++)
			{
				for (j = 0; j < 4; j++)
				{
					al_destroy_bitmap(note_bitmap_array[i][j]);
				}
			}
			al_destroy_bitmap(press_key);
			return -1;
		}
		al_set_target_bitmap(line_bitmap);
		al_clear_to_color(al_map_rgb(255, 255, 255));

		timer_bitmap = al_create_bitmap(SW_P_100, 10);
		if (!timer_bitmap)
		{
			al_show_native_message_box(allegro_display, "Error", "Error", "Failed to load timer_line!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
			al_destroy_display(allegro_display);
			al_destroy_timer(allegro_timer);
			for (i = 0; i < blocks_x; i++)
			{
				for (j = 0; j < 4; j++)
				{
					al_destroy_bitmap(note_bitmap_array[i][j]);
				}
			}
			al_destroy_bitmap(press_key);
			al_destroy_bitmap(line_bitmap);
			return -1;
		}
		al_set_target_bitmap(timer_bitmap);
		al_clear_to_color(al_map_rgb(255, 255, 0));

		timer_airplane = al_load_bitmap("black_plane_16.png");

		if (!timer_airplane)
		{
			al_show_native_message_box(allegro_display, "Error", "Error", "Failed to load black_plane_16.png!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
			al_destroy_display(allegro_display);
			al_destroy_timer(allegro_timer);
			for (i = 0; i < blocks_x; i++)
			{
				for (j = 0; j < 4; j++)
				{
					al_destroy_bitmap(note_bitmap_array[i][j]);
				}
			}
			al_destroy_bitmap(press_key);
			al_destroy_bitmap(line_bitmap);
			al_destroy_bitmap(timer_bitmap);
			return -1;
		}

		allegro_event_queue = al_create_event_queue();

		if (!allegro_event_queue)
		{
			al_show_native_message_box(allegro_display, "Error", "Error", "Failed to create event_queue", NULL, ALLEGRO_MESSAGEBOX_ERROR);
			al_destroy_display(allegro_display);
			al_destroy_timer(allegro_timer);
			for (i = 0; i < blocks_x; i++)
			{
				for (j = 0; j < 4; j++)
				{
					al_destroy_bitmap(note_bitmap_array[i][j]);
				}
			}
			al_destroy_bitmap(press_key);
			al_destroy_bitmap(line_bitmap);
			al_destroy_bitmap(timer_bitmap);
			al_destroy_bitmap(timer_airplane);
			return -1;
		}

		score_font = al_create_builtin_font();

		//Event 등록하는 부분
		{
			al_register_event_source(allegro_event_queue, al_get_display_event_source(allegro_display));

			al_register_event_source(allegro_event_queue, al_get_timer_event_source(allegro_timer));

			al_register_event_source(allegro_event_queue, al_get_keyboard_event_source());
		}
	}

	printf("TESTESTSETSEJFIEIFHOEI\n");

	//이경훈 초기화
	{
		font = al_load_font("NanumGothic.ttf", 20, NULL);
		title_bar = al_load_bitmap("Title_bar.png");
		title_Upward = al_load_bitmap("title_up_arrow.png");
		title_Downward = al_load_bitmap("title_down_arrow.png");
		track_Rightward = al_load_bitmap("track_right_arrow.png");
		track_Leftward = al_load_bitmap("track_left_arrow.png");
		album_picture = NULL;//앨범 사진
		start_button = al_load_bitmap("Start_Button.png");
		track_bar = al_load_bitmap("track_bar.png");

		//이벤트 큐 생성
		event_queue = al_create_event_queue();

		//이벤트 큐 등록
		al_register_event_source(event_queue, al_get_display_event_source(allegro_display));
		al_register_event_source(event_queue, al_get_mouse_event_source());
		al_register_event_source(event_queue, al_get_keyboard_event_source());
	}

	//이경훈 한번만 초기화 할 부분
	{
		if ((dp = opendir("..\\MIDI\\")) == NULL)
		{
			printf("error");
		}

		while ((dir = readdir(dp)) != NULL)
		{
			if ((strstr(dir->d_name, ".mid")) != 0)
			{
				if (strlen(dir->d_name) > max_file_strlen)
				{
					max_file_strlen = dir->d_namlen;
				}
				music_number = music_number + 1;
			}
		}

		closedir(dp);

		//memory allocation layer
		title = (char**)malloc(sizeof(char*) * (music_number + 2));
		for (i = 0; i < music_number + 2; i++)
		{
			title[i] = (char*)malloc(sizeof(char) * (max_file_strlen + 1));
			memset(title[i], 0, sizeof(char) * (max_file_strlen + 1));
		}
		picture_name = (char*)malloc(sizeof(char)*(max_file_strlen + 1 + strlen("..\\Album_Cover\\") + strlen(".jpg")));

		///testing
		if ((dp = opendir("..\\MIDI\\")) == NULL)
		{
			printf("error");
		}

		{
			strcpy(title[0], "- end -");
			int cnt = 0;
			while ((dir = readdir(dp)) != NULL)
			{
				if ((strstr(dir->d_name, ".mid")) != 0)
				{
					strncpy(title[cnt + 1], dir->d_name, strlen(dir->d_name) - 4);
					cnt++;
				}
			}
			strcpy(title[music_number + 1], "- end -");
		}
	}

	//FMOD초기화.
	{
		System_Create(&g_pSystem);
		g_pSystem->init(FMOD_MAX_CHANNELS, FMOD_INIT_NORMAL, 0);
		g_pChannel->setVolume(1);//volume을 1로 조정함으로써 최대 크기로 사용.
	}

	printf("TESTESTSETSEJFIEIFHOEI\n");

	while (1)
	{

		//게임의 한곡이 플레이 된 후 다시 변수를 초기화해야 할것들을 초기화하자!
		{
			allegro_timer_count = 0;
			score = 0;
			redraw = true;
			music_ending_time = -1;
			//memset(blocks, 0, sizeof(blocks));
			for (i = 0; i < blocks_y; i++)
			{
				memset(blocks[i], 0, sizeof(char) * blocks_x);
			}
			memset(key, 0, sizeof(key));
			FD_midi = NULL;
			CLEAR_MEMORY_MIDI_SETTING(&midi_setting);
			memset(&mthd_data, 0, sizeof(MTHD_DATA));
			mp3_file_name = NULL;
			mp3_file_name_len = 0;
			goto_menu = false;
			read_file_name = true;
			al_pause_event_queue(allegro_event_queue, true);
		}

		//이경훈 반복초기화
		{
			title_index = 0;
			track_num_index = 0;
			up = false;
			down = false;
			right = false;
			left = false;

			done = false;
			draw = true;
			
		}


		//메뉴 출력하기
		{
			//그리는 부분을 배경으로 하겠다는거
			al_set_target_bitmap(al_get_backbuffer(allegro_display));

			al_clear_to_color(al_map_rgb(0, 0, 0));

			al_flip_display();
		}

		while (!done)
		{
			al_wait_for_event(event_queue, &event);
			if (event.type == ALLEGRO_EVENT_KEY_DOWN)
			{
				draw = true;
				switch (event.keyboard.keycode)
				{
				case ALLEGRO_KEY_UP: // 곡 위로  up이나 down 이 트루면 music title index가 변함
					read_file_name = up = true;
					break;
				case ALLEGRO_KEY_DOWN: // 곡아래
					read_file_name = down = true;
					break;
				case ALLEGRO_KEY_RIGHT:// 트랙 오른쪽 right 나 left 가 true 면 track_num_str 의 track_num_index가 변함
					right = true;
					break;
				case ALLEGRO_KEY_LEFT://트랙 왼쪽
					left = true;
					break;
				}
			}
			else if (event.type == ALLEGRO_EVENT_KEY_UP)
			{
				switch (event.keyboard.keycode)
				{
				case ALLEGRO_KEY_ESCAPE:
					if (timestamp_esc == event.keyboard.timestamp)
					{
						break;
					}
					//printf("%lf\n", event.keyboard.timestamp);
					done = true;
					end_program = true;
					break;

				case ALLEGRO_KEY_ENTER: // start
					if (timestamp_enter == event.keyboard.timestamp)
					{
						break;
					}
					finish_game = false;
					done = true;
					break;
				}
			}

			else if (event.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) // 마우스 클릭
			{

				if (event.mouse.button == 1)
				{
					draw = true;

					if ((230 <= event.mouse.x&&event.mouse.x <= 280) && (635 <= event.mouse.y&&event.mouse.y <= 705)) // downward click
					{
						down = true;
					}
					else if ((230 <= event.mouse.x&&event.mouse.x <= 280) && (385 <= event.mouse.y&&event.mouse.y <= 455))// upward click
					{
						up = true;
					}
					else if ((42 <= event.mouse.x&&event.mouse.x <= 92) && (735 <= event.mouse.y&&event.mouse.y <= 770))//left click
					{
						left = true;
					}
					else if ((219 <= event.mouse.x&&event.mouse.x <= 269) && (735 <= event.mouse.y&&event.mouse.y <= 770))//right click
					{
						right = true;
					}
					else if ((297 <= event.mouse.x&&event.mouse.x <= 430) && (744 <= event.mouse.y&&event.mouse.y <= 811)) // start click
					{
						done = true;
					}
				}
			}

			if (draw)
			{
				draw = false;

				// 곡목록 조정
				if (up)
				{
					up = false;
					if (title_index != 0)//밑에 더 없을때 고정
					{
						track_num_index = 0;
						title_index = title_index - 1;
					}
				}
				else if (down)
				{
					down = false;
					if ((title_index + 2) != music_number + 1)
					{
						track_num_index = 0;
						title_index = title_index + 1;
					}
				}


				/*
				char track_num_str[3];
				for (int i = 0; i < 3; i++)
				{
				itoa(i + 1, track_num_str, 10);
				al_draw_text(font, al_map_rgb(0, 0, 0), 112 + (i * 35), 746, ALLEGRO_ALIGN_LEFT, track_num_str);
				}
				}
				*/
				al_draw_bitmap(track_bar, 102, 686, NULL); // 트랙 숫자보다 위에 있어야 글자가 출력됨`
														   //리스트 가운데 있는 노래이름 받아오기(앨범 사진 출력할때 필요)
				strcpy(picture_name, "..\\Album_Cover\\");
				strcat(picture_name, title[title_index + 1]);
				strcat(picture_name, ".png");//확장명 추가
											 //여기서 mtrk_cnt를 읽음!
											 //파일명은 picture_name.mid로함.
											 //mthd_data.mtrk_cnt;
				if(read_file_name)
				{
					//2016_11_09_18_23
					read_file_name = false;
					midi_file_name = (char*)calloc(strlen(title[title_index + 1]) + strlen("../MIDI/") + strlen(".mid") + 1, sizeof(char));
					strcpy(midi_file_name, "../MIDI/");
					strcat(midi_file_name, title[title_index + 1]);
					strcat(midi_file_name, ".mid");

					FD_midi = NULL;

					fopen_s(&FD_midi, midi_file_name, "rb");
					printf("file_name : %s\n", midi_file_name);//DEBUG_LINE

					if (NULL == FD_midi)
					{
						perror("OEPN FILE ERROR!\n");
						return -1;
					}
					GET_FILE_SIZE(FD_midi);

					//MTHD 부분
					{
						for (i = 0; i < 4; i++)
						{
							midi_chunk_type[i] = fgetc(FD_midi);
						}
						if (strcmp(midi_chunk_type, "MThd") == 0)
						{
							MTHD_FUNCTION(FD_midi, &mthd_data);
						}
						else
						{
							printf("OPEN_MThd_ERROR!\n");
						}
					}

					fclose(FD_midi);
					free(midi_file_name);
					//2016_11_09 완료

					for (i = 0; i < mthd_data.mtrk_cnt; i++) // 임시로 track_을  track 의 개수로 사용함
					{
						itoa(i + 1, track_num_str[i + 1], 10); // track_num_str[1] ="1" 
					}
					strcpy(track_num_str[0], "-"); //서로 양쪽 끝"-"
					strcpy(track_num_str[mthd_data.mtrk_cnt + 1], "-");
				}

				//strncpy_s(midi_file_name, midi_file_name_len, "../MIDI/Wings Of Piano.mid", midi_file_name_len - 1);

				if (right == true)
				{
					right = false;
					if (track_num_index + 2 != mthd_data.mtrk_cnt + 1)
						track_num_index = track_num_index + 1;
				}
				else if (left == true)
				{
					left = false;
					if (track_num_index != 0)
						track_num_index = track_num_index - 1;
				}
				//트랙 넘버 출력
				al_draw_text(font, al_map_rgb(0, 0, 0), 112 + (0 * 35), 746, ALLEGRO_ALIGN_LEFT, track_num_str[track_num_index]);
				al_draw_text(font, al_map_rgb(0, 0, 0), 112 + (1 * 35), 746, ALLEGRO_ALIGN_LEFT, track_num_str[track_num_index + 1]); // 가운데에 있는 번호
				al_draw_text(font, al_map_rgb(0, 0, 0), 112 + (2 * 35), 746, ALLEGRO_ALIGN_LEFT, track_num_str[track_num_index + 2]);

				if ((album_picture = al_load_bitmap(picture_name)) != NULL)
				{
					al_draw_scaled_bitmap(album_picture, 0, 0, al_get_bitmap_width(album_picture), al_get_bitmap_height(album_picture), 100, 50, 310, 310, NULL);

				}
				else//사진 불러오는데 실패 했을때 (존재 하지 않을때?)
				{
					album_picture = al_load_bitmap("NotAlbum.png");
					al_draw_scaled_bitmap(album_picture, 0, 0, al_get_bitmap_width(album_picture), al_get_bitmap_height(album_picture), 100, 50, 310, 310, NULL);

				}


				//al_draw_scaled_bitmap(album_picture[i], 0, 0, al_get_bitmap_width(album_picture[i]), al_get_bitmap_height(album_picture[i]), 0, 0, 200, 200, NULL);
				//delta 만큼 색깔 변화
				//al_draw_scaled_bitmap(Upward, 0, 0, al_get_bitmap_width(Upward), al_get_bitmap_height(Upward), 138, 644, 35, 49, NULL);
				//al_draw_scaled_bitmap(Downward, 0, 0, al_get_bitmap_width(Downward), al_get_bitmap_height(Downward), 138, 819, 35, 49, NULL);
				al_draw_bitmap(title_bar, 55, 470, NULL);
				//al_draw_bitmap(track_bar, 85, 732, NULL);
				al_draw_bitmap(track_Leftward, 43, 739, NULL);
				al_draw_bitmap(track_Rightward, 218, 739, NULL);
				al_draw_bitmap(title_Upward, 230, 385, NULL);
				al_draw_bitmap(title_Downward, 230, 635, NULL);
				al_draw_bitmap(start_button, 295, 745, NULL);
				al_draw_text(font, al_map_rgb(0, 0, 0), 55, 490, ALLEGRO_ALIGN_LEFT, title[title_index]);
				al_draw_text(font, al_map_rgb(0, 0, 0), 55, 540, ALLEGRO_ALIGN_LEFT, title[title_index + 1]);
				al_draw_text(font, al_map_rgb(0, 0, 0), 55, 590, ALLEGRO_ALIGN_LEFT, title[title_index + 2]);
				al_flip_display();
				al_clear_to_color(al_map_rgb(0, 0, 0));
				al_destroy_bitmap(album_picture);
			}
		}


		if (end_program)
		{
			break;
		}
		//메모리 초기화
		{
			FD_midi = NULL;
			memset(&mthd_data, 0, sizeof(MTHD_DATA));
		}

		//MIDI 파일 여는 부분.
		{
			//midi_file_name_len = strlen("../MIDI/Wings Of Piano.mid") + 1;
			midi_file_name_len = strlen("../MIDI/") + strlen(".mid") + strlen(title[title_index + 1]) + 1;

			midi_file_name = (char*)calloc(midi_file_name_len, sizeof(char));

			//strncpy_s(midi_file_name, midi_file_name_len, "../MIDI/Wings Of Piano.mid", midi_file_name_len - 1);

			strcpy(midi_file_name, "../MIDI/");
			strcat(midi_file_name, title[title_index + 1]);
			strcat(midi_file_name, ".mid");

			fopen_s(&FD_midi, midi_file_name, "rb");

			printf("file_name : %s\n", midi_file_name);//DEBUG_LINE

			if (NULL == FD_midi)
			{
				perror("OEPN FILE ERROR!\n");
				return -1;
			}
		}

		//MIDI 설정 부분
		//MIDI파일을 통해 노래의 총 길이를 찾아보는 구간이기도 하다.
		{
			GET_FILE_SIZE(FD_midi);//file_size를 리턴한다. 필요할 경우 사용하자.

			//MTHD 부분
			{
				for (i = 0; i < 4; i++)
				{
					midi_chunk_type[i] = fgetc(FD_midi);
				}
				if (strcmp(midi_chunk_type, "MThd") == 0)
				{
					MTHD_FUNCTION(FD_midi, &mthd_data);
				}
				else
				{
					printf("OPEN_MThd_ERROR!\n");
				}
			}

			//초당 틱수 설정
			{
				midi_setting.SecondsPerTick = 500000.0f / 1000000.0f / float(mthd_data.ticks_per_quater_note);
			}

			//메모리 초기화
			{
				FD_track_arr = (FILE**)malloc(sizeof(FILE*)*(mthd_data.mtrk_cnt));//track의 개수만큼 메모리 할당.
				//midi_delta_time_arr = (uint64_t*)calloc(mthd_data.mtrk_cnt, sizeof(uint64_t));
				//midi_Full_delta_time_arr = (uint64_t*)calloc(mthd_data.mtrk_cnt, sizeof(uint64_t));
				mtrk_data = (MTRK_DATA*)calloc(mthd_data.mtrk_cnt, sizeof(MTRK_DATA));
				midi_timer = (double*)calloc(mthd_data.mtrk_cnt, sizeof(double));
				midi_queue_arr = (int*)calloc(mthd_data.mtrk_cnt, sizeof(int));
				track_blocks = (char(**)[blocks_x])calloc(mthd_data.mtrk_cnt, sizeof(char**));
				track_timer = (double*)calloc(mthd_data.mtrk_cnt, sizeof(double));
			}

			//MTRK시작부분 찾기
			for (i = 0; i < mthd_data.mtrk_cnt; i++)
			{
				FIND_NEXT_MTRK(FD_midi);
				fopen_s(&FD_track_arr[i], midi_file_name, "rb");
				fseek(FD_track_arr[i], ftell(FD_midi), SEEK_SET);
				printf("fseek : %d\n", ftell(FD_track_arr[i]));//DEBUG_LINE
			}

			//DEBUG_LINE
			printf("DEBUG_LINE_2016_11_08\n");

			/*
			/*MTRK_DATA_SIZE를 읽어야함!
			/*DATA_SIZE를 읽어서 쓸일은 없겠지만 4byte만큼의 블럭을 읽어야 하므로 사용한다.
			/*그 이유는 FF 2F 00을 가지고 TRACK의 끝을 판단하기 때문이다.
			*/
			for (i = 0; i < mthd_data.mtrk_cnt; i++)
			{
				GET_DATA_SIZE(FD_track_arr[i]);
			}

			midi_setting.left_mtrk_cnt = mthd_data.mtrk_cnt;

			for (i = 0; i < mthd_data.mtrk_cnt; i++)
			{
				midi_queue_arr[i] = i;
			}

			//DELTA_TIME을 먼저 확인해야함.
			for (i = 0; i < mthd_data.mtrk_cnt; i++)
			{
				READ_DELTA_TIME(FD_track_arr[i], &(midi_timer[i]), &(midi_setting));
				//위에서 DELTA_TIME을 읽은 만큼 TIMER에 추가.
				track_timer[i] += midi_timer[i];
			}


			//음악의 총 길이를 저장하는 부분
			{
				bool music_end = false;
				while (!music_end)
				{
					while (midi_setting.EndFlag != mthd_data.mtrk_cnt && 0 >= midi_timer[midi_queue_arr[midi_setting.EndFlag]])
					{
						//MTRK_FUNCTION(FD_track_arr[midi_queue_arr[midi_setting.EndFlag]], &(mtrk_data[midi_queue_arr[midi_setting.EndFlag]]), &(midi_timer[midi_queue_arr[midi_setting.EndFlag]]), &(blocks[0][0]), &(midi_setting), &(mthd_data));
						READ_MUSIC_TIME(FD_track_arr[midi_queue_arr[midi_setting.EndFlag]], &(mtrk_data[midi_queue_arr[midi_setting.EndFlag]]), &(midi_timer[midi_queue_arr[midi_setting.EndFlag]]), &(midi_setting), &(mthd_data));

						//끝을 알아보는 부분
						if (midi_setting.EndFlag == mthd_data.mtrk_cnt)
						{
							//DEBUG_LINE
							printf("READ_MUSIC_TIME_FINISH!\n");
							music_end = true;
							break;
						}

						//track_timer에 delta_time의 시간값을 더하는것.
						track_timer[midi_queue_arr[midi_setting.EndFlag]] += midi_timer[midi_queue_arr[midi_setting.EndFlag]];

						//가장 낮은 timer를 가진 Track이 queue의 맨 앞에 오도록 설정.
						for (j = midi_setting.EndFlag; j < mthd_data.mtrk_cnt - 1; j++)
						{
							for (i = midi_setting.EndFlag; i < mthd_data.mtrk_cnt - j + midi_setting.EndFlag - 1; i++)
							{
								if (midi_timer[midi_queue_arr[i]] > midi_timer[midi_queue_arr[i + 1]])
								{
									tmp_midi_queue = midi_queue_arr[i];
									midi_queue_arr[i] = midi_queue_arr[i + 1];
									midi_queue_arr[i + 1] = tmp_midi_queue;
								}
							}
						}
						//printf("DEBUG_LINE:331\n");//DEBUG_LINE
						//printf("Setting.EndFlag : %d\nMTHD.mtrk_cnt : %d\n", midi_setting.EndFlag, mthd_data.mtrk_cnt);//DEBUG_LINE6
					}

					//  1.0/FPS 만큼의 시간이 지났으므로 모든 track의 timer가 흘러야함.
					for (i = midi_setting.EndFlag; i < mthd_data.mtrk_cnt; i++)
					{
						midi_timer[midi_queue_arr[i]] -= 1.0 / FPS;
					}
				}
			}

			//각 트랙의 길이중 가장 긴 부분을 저장하는 부분
			{
				midi_timer_end = track_timer[0];
				for (i = 1; i < mthd_data.mtrk_cnt; i++)
				{
					if (midi_timer_end < track_timer[i])
					{
						midi_timer_end = track_timer[i];
					}
				}
				//Allegro의 Frame단위로 시간을 바꿈!
				midi_timer_end_allegro_time = midi_timer_end * FPS / 100;//100을 나눠주는 이유는 100퍼센트로 따져서 비행기의 위치를 옮겨야 하기 때문이다!
			}

			//열었던 파일들 다시 닫는다.
			{
				for (i = 0; i < mthd_data.mtrk_cnt; i++)
				{
					fclose(FD_track_arr[i]);
				}
			}

			//DEBUG_LINE
			{
				for (i = 0; i < mthd_data.mtrk_cnt; i++)
				{
					printf("TRACK %d FULL_LENGTH : %lf\n", i, track_timer[i]);
				}
				printf("midi_timer_end : %lf\n", midi_timer_end);
			}
		}

		//DEBUG_LINE
		printf("DEBUG_LINE_2016_11_08\n");

		//MIDI 설정 부분 다시 초기화
		{
			//음악의 길이를 읽으면서 사용한 메모리를 초기화하자!
			//파일포인터의 위치도 원래대로 초기화!
			{
				fseek(FD_midi, 0, SEEK_SET);//FD_midi를 처음으로!
				//memset(&FD_track_arr[0], 0, sizeof(FILE*) * mthd_data.mtrk_cnt);
				memset(&mtrk_data[0], 0, sizeof(MTRK_DATA) * mthd_data.mtrk_cnt);
				memset(&midi_timer[0], 0, sizeof(double) * mthd_data.mtrk_cnt);
				memset(&midi_queue_arr[0], 0, sizeof(int) * mthd_data.mtrk_cnt);
				//track_blocks의 경우 음악의 길이를 읽으면서 사용하지 않았으므로 초기화 하지 않음.
				//track_timer의 경우 재사용할 이유가 없기 때문에 초기화 하지 않음.
				CLEAR_MEMORY_MIDI_SETTING(&midi_setting);
				//mthd_data를 가장 마지막으로 초기화 하는 이유는 mthd_data.mtrk_cnt를 마지막으로 초기화 해야 위의 memset이 정상 작동하기 때문이다.
				memset(&mthd_data, 0, sizeof(MTHD_DATA));
			}

			//MTHD 부분
			{
				for (i = 0; i < 4; i++)
				{
					midi_chunk_type[i] = fgetc(FD_midi);
				}
				if (strcmp(midi_chunk_type, "MThd") == 0)
				{
					MTHD_FUNCTION(FD_midi, &mthd_data);
				}
				else
				{
					printf("OPEN_MThd_ERROR!\n");
				}
			}

			//초당 틱수 설정
			{
				midi_setting.SecondsPerTick = 500000.0f / 1000000.0f / float(mthd_data.ticks_per_quater_note);
			}

			//MTRK시작부분 찾기
			for (i = 0; i < mthd_data.mtrk_cnt; i++)
			{
				FIND_NEXT_MTRK(FD_midi);
				fopen_s(&FD_track_arr[i], midi_file_name, "rb");
				fseek(FD_track_arr[i], ftell(FD_midi), SEEK_SET);
				printf("fseek : %d\n", ftell(FD_track_arr[i]));//DEBUG_LINE
			}

			/*
			/*MTRK_DATA_SIZE를 읽어야함!
			/*DATA_SIZE를 읽어서 쓸일은 없겠지만 4byte만큼의 블럭을 읽어야 하므로 사용한다.
			/*그 이유는 FF 2F 00을 가지고 TRACK의 끝을 판단하기 때문이다.
			*/
			for (i = 0; i < mthd_data.mtrk_cnt; i++)
			{
				GET_DATA_SIZE(FD_track_arr[i]);
			}

			midi_setting.left_mtrk_cnt = mthd_data.mtrk_cnt;

			for (i = 0; i < mthd_data.mtrk_cnt; i++)
			{
				midi_queue_arr[i] = i;
			}

			//DELTA_TIME을 먼저 확인해야함.
			for (i = 0; i < mthd_data.mtrk_cnt; i++)
			{
				READ_DELTA_TIME(FD_track_arr[i], &(midi_timer[i]), &(midi_setting));
			}
		}

		//음악의 track을 선택하는 부분. 고쳐야할점이 많다!
		{
			//DEBUG_LINE!!!!!---------나중에 고쳐야 되는 부분!-----------------------------------------------------------------------------------------------------
			for (i = 0; i < mthd_data.mtrk_cnt; i++)
			{
				track_blocks[i] = garbage_blocks;
			}
			track_blocks[atoi(track_num_str[track_num_index + 1])-1] = blocks;
			//track_blocks[0] = blocks;
		}

		//FMOD에 MP3파일을 추가시키는 부분.
		{
			mp3_file_name_len = strlen("../MP3/") + strlen(".mp3") + strlen(title[title_index + 1]) + 1;
			mp3_file_name = (char*)calloc(mp3_file_name_len, sizeof(char));
			strcpy(mp3_file_name, "../MP3/");
			strcat(mp3_file_name, title[title_index + 1]);
			strcat(mp3_file_name, ".mp3");
			g_pSystem->createSound(mp3_file_name, FMOD_HARDWARE, 0, &g_pSound[0]);
		}

		//그리는 부분을 배경으로 하겠다는거
		{
			al_set_target_bitmap(al_get_backbuffer(allegro_display));

			al_clear_to_color(al_map_rgb(0, 0, 0));

			al_flip_display();

			al_set_timer_count(allegro_timer, (int64_t)(0));//알레그로 타이머를 0으로 초기화
			//al_flush_event_queue(allegro_event_queue);
			//al_register_event_source(allegro_event_queue, al_get_timer_event_source(allegro_timer));
			if (done)
			{
				al_start_timer(allegro_timer);
				al_pause_event_queue(allegro_event_queue, false);
			}
		}

		//printf("Setting.EndFlag : %d\nMTHD.mtrk_cnt : %d\n", midi_setting.EndFlag, mthd_data.mtrk_cnt);//DEBUG_LINE

		//printf("DEBUG_LINE : print allegro timer count : ", al_get_timer_count(allegro_timer));

		while (!finish_game)
		{
			ALLEGRO_EVENT allegro_ev;
			al_wait_for_event(allegro_event_queue, &allegro_ev);

			//타이머가 돌아갈 때
			if (allegro_ev.type == ALLEGRO_EVENT_TIMER)
			{
				//FPS/4를 한 이유는 1/NPS초 마다 노트를 생성하기 위해서.
				if (al_get_timer_count(allegro_timer) % (int)(FPS / NPS) == 0)
				{
					//노트 밀어내는 부분
					for (i = blocks_y - 1; i > 0; i--)
					{
						for (j = 0; j < blocks_x; j++)
						{
							blocks[i][j] = blocks[i - 1][j];
						}
					}

					//밀어낸 이후 첫줄 초기화

					for (i = 0; i < blocks_x; i++)
					{
						blocks[0][i] = false;
					}

					for (i = 0; i < blocks_x; i++)
					{
						if (blocks[5][i])
						{
							if (blocks[4][i])
							{
								blocks[5][i] = 4;//long_note_down
								if (blocks[6][i])
								{
									blocks[5][i] = 3;//long_note_center
								}
							}
							else if (blocks[6][i])
							{
								blocks[5][i] = 2;//long_note_up
							}

						}
					}

					//음악 출력 시기 생성
					//예제 음악이 바로 시작을 안해서 뒤에 숫자를 바꿈.
					if (al_get_timer_count(allegro_timer) == (blocks_y / NPS - 0.3)*FPS)
					{
						g_pSystem->playSound(FMOD_CHANNEL_FREE, g_pSound[SELECTED_MUSIC], 0, &g_pChannel);
					}

					//FMOD 음악 업데이트 부분
					g_pSystem->update();
				}
				//printf("2");//DEBUG_LINE
				//MIDI를 읽는 부분
				//조건 순서 변경하지 말것!
				//midi_timer[midi_queue_arr[midi_setting.EndFlag]]부분에서 overflow 생김.
				while (midi_setting.EndFlag != mthd_data.mtrk_cnt && 0 >= midi_timer[midi_queue_arr[midi_setting.EndFlag]])
				{
					//MTRK_FUNCTION(FD_track_arr[midi_queue_arr[midi_setting.EndFlag]], &(mtrk_data[midi_queue_arr[midi_setting.EndFlag]]), &(midi_timer[midi_queue_arr[midi_setting.EndFlag]]), &(blocks[0][0]), &(midi_setting), &(mthd_data));
					MTRK_FUNCTION(FD_track_arr[midi_queue_arr[midi_setting.EndFlag]], &(mtrk_data[midi_queue_arr[midi_setting.EndFlag]]), &(midi_timer[midi_queue_arr[midi_setting.EndFlag]]), &(track_blocks[midi_queue_arr[midi_setting.EndFlag]][0][0]), &(midi_setting), &(mthd_data));

					//가장 낮은 timer를 가진 Track이 queue의 맨 앞에 오도록 설정.
					for (j = midi_setting.EndFlag; j < mthd_data.mtrk_cnt - 1; j++)
					{
						for (i = midi_setting.EndFlag; i < mthd_data.mtrk_cnt - j + midi_setting.EndFlag - 1; i++)
						{
							if (midi_timer[midi_queue_arr[i]] > midi_timer[midi_queue_arr[i + 1]])
							{
								tmp_midi_queue = midi_queue_arr[i];
								midi_queue_arr[i] = midi_queue_arr[i + 1];
								midi_queue_arr[i + 1] = tmp_midi_queue;
							}
						}
					}
					//printf("DEBUG_LINE:331\n");//DEBUG_LINE
					//printf("Setting.EndFlag : %d\nMTHD.mtrk_cnt : %d\n", midi_setting.EndFlag, mthd_data.mtrk_cnt);//DEBUG_LINE6
				}

				//끝을 알아보는 부분
				if (midi_setting.EndFlag == mthd_data.mtrk_cnt)
				{
					if (music_ending_time == 0)
					{
						//g_pSound[SELECTED_MUSIC]->is
						finish_game = true;
					}
					else if (music_ending_time == -1)
					{
						printf("DEBUG_LINE : SAME!\n");//DEBUG_LINE
						music_ending_time = (blocks_y / NPS + 5)*FPS;//5초의 시간을 추가로 공급.
					}
					else
					{
						music_ending_time -= 1;
					}
					//finish_game = true;
					//g_pChannel->stop();
					//break;
				}

				//  1.0/FPS 만큼의 시간이 지났으므로 모든 track의 timer가 흘러야함.
				for (i = midi_setting.EndFlag; i < mthd_data.mtrk_cnt; i++)
				{
					midi_timer[midi_queue_arr[i]] -= 1.0 / FPS;
				}

				redraw = true;
			}

			//윈도우 창을 닫는 경우
			if (allegro_ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
			{
				break;
			}

			if (allegro_ev.type == ALLEGRO_EVENT_KEY_DOWN)
			{
				switch (allegro_ev.keyboard.keycode)
				{
				case ALLEGRO_KEY_Q:
					key[KEY_Q] = true;
					break;
				case ALLEGRO_KEY_W:
					key[KEY_W] = true;
					break;
				case ALLEGRO_KEY_E:
					key[KEY_E] = true;
					break;
				case ALLEGRO_KEY_R:
					key[KEY_R] = true;
					break;
				case ALLEGRO_KEY_T:
					key[KEY_T] = true;
					break;
				case ALLEGRO_KEY_Y:
					key[KEY_Y] = true;
					break;
				case ALLEGRO_KEY_U:
					key[KEY_U] = true;
					break;
					//없애는 키 부분
					/*
					case ALLEGRO_KEY_I:
						key[KEY_I] = true;
						break;
					case ALLEGRO_KEY_O:
						key[KEY_O] = true;
						break;
					case ALLEGRO_KEY_P:
						key[KEY_P] = true;
						break;
					case ALLEGRO_KEY_OPENBRACE:
						key[KEY_OPENBRACE] = true;
						break;
					case ALLEGRO_KEY_CLOSEBRACE:
						key[KEY_CLOSEBRACE] = true;
						break;
					*/
				}
			}

			if (allegro_ev.type == ALLEGRO_EVENT_KEY_UP)
			{
				switch (allegro_ev.keyboard.keycode)
				{
				case ALLEGRO_KEY_Q:
					key[KEY_Q] = false;
					break;
				case ALLEGRO_KEY_W:
					key[KEY_W] = false;
					break;
				case ALLEGRO_KEY_E:
					key[KEY_E] = false;
					break;
				case ALLEGRO_KEY_R:
					key[KEY_R] = false;
					break;
				case ALLEGRO_KEY_T:
					key[KEY_T] = false;
					break;
				case ALLEGRO_KEY_Y:
					key[KEY_Y] = false;
					break;
				case ALLEGRO_KEY_U:
					key[KEY_U] = false;
					break;
				case ALLEGRO_KEY_ESCAPE:
					finish_game = true;
					goto_menu = true;
					//printf("%lf\n", allegro_ev.keyboard.timestamp);
					timestamp_esc = allegro_ev.keyboard.timestamp;
					break;
					//없애는 키 부분
					/*
					case ALLEGRO_KEY_I:
						key[KEY_I] = false;
						break;
					case ALLEGRO_KEY_O:
						key[KEY_O] = false;
						break;
					case ALLEGRO_KEY_P:
						key[KEY_P] = false;
						break;
					case ALLEGRO_KEY_OPENBRACE:
						key[KEY_OPENBRACE] = false;
						break;
					case ALLEGRO_KEY_CLOSEBRACE:
						key[KEY_CLOSEBRACE] = false;
						break;
					*/
				}
			}

			//모든 큐가 비워져서 윈도우를 새로 그릴 경우
			if (redraw && al_is_event_queue_empty(allegro_event_queue))
			{
				redraw = false;
				allegro_timer_count = al_get_timer_count(allegro_timer);
				hit_score = allegro_timer_count % (int64_t)(FPS / NPS);//중앙으로부터 얼마나 떨어져 있는지를 보여준다. FPS/NPS를 한 이유는 1/NPS초 마다 노트가 생성되기 때문이다.
				delta = hit_score + 50;//FPS/NPS를 한 이유는 1/NPS초 마다 생성된 노트를 출력하기 위해서. +50을 한 이유는 LINE에서 생성되게 하기 위해서.
				if (hit_score > hit_line)
					hit_score = hit_line * 2 - hit_score;
				//키 입력시 BITMAP출력을 위해 배경을 먼저 지움.
				al_clear_to_color(al_map_rgb(0, 0, 0));

				//키입력 관련 부분
				{
					if (key[KEY_Q])
					{
						al_draw_bitmap(press_key, KEY_Q * note_size_x + 10, blocks_y_screen * note_size_y, 0);
						if (blocks[blocks_y - 1][KEY_Q])
						{
							if (blocks[blocks_y - 1][KEY_Q] == 3)
							{
								score += hit_line;
							}
							else
							{
								score += hit_score;
							}
							blocks[blocks_y - 1][KEY_Q] = false;
						}
					}
					if (key[KEY_W])
					{
						al_draw_bitmap(press_key, KEY_W * note_size_x + 10, blocks_y_screen * note_size_y, 0);
						if (blocks[blocks_y - 1][KEY_W])
						{
							if (blocks[blocks_y - 1][KEY_W] == 3)
							{
								score += hit_line;
							}
							else
							{
								score += hit_score;
							}
							blocks[blocks_y - 1][KEY_W] = false;
						}
					}
					if (key[KEY_E])
					{
						al_draw_bitmap(press_key, KEY_E * note_size_x + 10, blocks_y_screen * note_size_y, 0);
						if (blocks[blocks_y - 1][KEY_E])
						{
							if (blocks[blocks_y - 1][KEY_E] == 3)
							{
								score += hit_line;
							}
							else
							{
								score += hit_score;
							}
							blocks[blocks_y - 1][KEY_E] = false;
						}
					}
					if (key[KEY_R])
					{
						al_draw_bitmap(press_key, KEY_R * note_size_x + 10, blocks_y_screen * note_size_y, 0);
						if (blocks[blocks_y - 1][KEY_R])
						{
							if (blocks[blocks_y - 1][KEY_R] == 3)
							{
								score += hit_line;
							}
							else
							{
								score += hit_score;
							}
							blocks[blocks_y - 1][KEY_R] = false;
						}
					}
					if (key[KEY_T])
					{
						al_draw_bitmap(press_key, KEY_T * note_size_x + 10, blocks_y_screen * note_size_y, 0);
						if (blocks[blocks_y - 1][KEY_T])
						{
							if (blocks[blocks_y - 1][KEY_T] == 3)
							{
								score += hit_line;
							}
							else
							{
								score += hit_score;
							}
							blocks[blocks_y - 1][KEY_T] = false;
						}
					}
					if (key[KEY_Y])
					{
						al_draw_bitmap(press_key, KEY_Y * note_size_x + 10, blocks_y_screen * note_size_y, 0);
						if (blocks[blocks_y - 1][KEY_Y])
						{
							if (blocks[blocks_y - 1][KEY_Y] == 3)
							{
								score += hit_line;
							}
							else
							{
								score += hit_score;
							}
							blocks[blocks_y - 1][KEY_Y] = false;
						}
					}
					if (key[KEY_U])
					{
						al_draw_bitmap(press_key, KEY_U * note_size_x + 10, blocks_y_screen * note_size_y, 0);
						if (blocks[blocks_y - 1][KEY_U])
						{
							if (blocks[blocks_y - 1][KEY_U] == 3)
							{
								score += hit_line;
							}
							else
							{
								score += hit_score;
							}
							blocks[blocks_y - 1][KEY_U] = false;
						}
					}
					//없애는 키 부분.
					/*
					if (key[KEY_I])
					{
						al_draw_bitmap(press_key, KEY_I * note_size_x + 10, blocks_y_screen * note_size_y, 0);
						if (blocks[blocks_y - 1][KEY_I])
						{
							blocks[blocks_y - 1][KEY_I] = false;
							score += hit_score;
						}
					}
					if (key[KEY_O])
					{
						al_draw_bitmap(press_key, KEY_O * note_size_x + 10, blocks_y_screen * note_size_y, 0);
						if (blocks[blocks_y - 1][KEY_O])
						{
							blocks[blocks_y - 1][KEY_O] = false;
							score += hit_score;
						}
					}
					if (key[KEY_P])
					{
						al_draw_bitmap(press_key, KEY_P * note_size_x + 10, blocks_y_screen * note_size_y, 0);
						if (blocks[blocks_y - 1][KEY_P])
						{
							blocks[blocks_y - 1][KEY_P] = false;
							score += hit_score;
						}
					}
					if (key[KEY_OPENBRACE])
					{
						al_draw_bitmap(press_key, KEY_OPENBRACE * note_size_x + 10, blocks_y_screen * note_size_y, 0);
						if (blocks[blocks_y - 1][KEY_OPENBRACE])
						{
							blocks[blocks_y - 1][KEY_OPENBRACE] = false;
							score += hit_score;
						}
					}
					if (key[KEY_CLOSEBRACE])
					{
						al_draw_bitmap(press_key, KEY_CLOSEBRACE * note_size_x + 10, blocks_y_screen * note_size_y, 0);
						if (blocks[blocks_y - 1][KEY_CLOSEBRACE])
						{
							blocks[blocks_y - 1][KEY_CLOSEBRACE] = false;
							score += hit_score;
						}
					}
					*/
				}

				//al_clear_to_color(al_map_rgb(0, 0, 0));

				//delta = al_get_timer_count(allegro_timer) % (int)(FPS/NPS) + 50;//FPS/NPS를 한 이유는 1/NPS초 마다 생성된 노트를 출력하기 위해서. +50을 한 이유는 LINE에서 생성되게 하기 위해서.
				for (i = 0; i < blocks_y_screen; i++)
				{
					for (j = 0; j < blocks_x; j++)
					{
						if (blocks[i + (blocks_y - blocks_y_screen)][j])
						{
							al_draw_bitmap(note_bitmap_array[j][blocks[i + (blocks_y - blocks_y_screen)][j] - 1], j * note_size_x + 10, i * note_size_y + delta, 0);//x좌표에 10을 추가한 이유는 좌우측 10여백을 줌으로써 보기 좋으라고.
						}
					}
				}

				//비행기 그리는 부분
				{
					for (i = 0; i < allegro_timer_count / midi_timer_end_allegro_time; i++)
					{
						al_draw_bitmap(timer_bitmap, i * SW_P_100, 37, 0);
					}
					al_draw_bitmap(timer_airplane, (i - 1) * SW_P_100, 34, 0);
				}


				al_draw_bitmap(line_bitmap, 0, 50, 0);
				al_draw_bitmap(line_bitmap, 0, bottom_line, 0);

				//score 출력부분
				//우선 DEBUG를 위해 allegro_timer_count를 출력하기로함.
				_itoa_s(score, score_str, 10);
				//_itoa_s((int)allegro_timer_count, score_str, 10);
				al_draw_text(score_font, al_map_rgb(255, 255, 255), SCREEN_W - 150, 40, ALLEGRO_ALIGN_CENTER, score_str);

				al_flip_display();
			}
		}

		if (finish_game && !(goto_menu))
		{
			ALLEGRO_EVENT allegro_ev;
			int key;

			al_clear_to_color(al_map_rgb(0, 0, 0));
			al_draw_text(score_font, al_map_rgb(255, 255, 255), SCREEN_W / 2, SCREEN_H / 2, ALLEGRO_ALIGN_CENTER, "END");
			al_flip_display();
			while (1)
			{
				al_wait_for_event(allegro_event_queue, &allegro_ev);
				if (allegro_ev.type == ALLEGRO_EVENT_KEY_UP)
				{
					key = allegro_ev.keyboard.keycode;
					if (key == ALLEGRO_KEY_ENTER)
					{
						timestamp_enter = allegro_ev.keyboard.timestamp;
						break;
					}
				}
			}
		}
		
		al_pause_event_queue(allegro_event_queue, true);
		al_stop_timer(allegro_timer);
		//al_unregister_event_source(allegro_event_queue,al_get_timer_event_source(allegro_timer));
		//MIDI 관련 메모리 해제
		{
			for (i = 0; i < mthd_data.mtrk_cnt; i++)
			{
				fclose(FD_track_arr[i]);
			}
			free(FD_track_arr);//track_arr 메모리 해제.
			free(midi_file_name);
			free(mtrk_data);
			free(midi_timer);
			free(midi_queue_arr);
			free(track_blocks);
			free(track_timer);//사실 이거 중간에 해제해도 되긴 하는데 그럴경우 매우 많이 헷갈리기에 한꺼번에 몰아서 해제함.
			free(mp3_file_name);
		}

		//FMOD Sound 관련 메모리 해제.
		{
			g_pSound[SELECTED_MUSIC]->release();
		}

	}

	//이부분은 프로그램이 종료되었을 때만 해제하자.
	{
		//Allegro 관련 메모리 해제
		{
			al_destroy_display(allegro_display);
			al_destroy_timer(allegro_timer);
			for (i = 0; i < blocks_x; i++)
			{
				for (j = 0; j < 4; j++)
				{
					al_destroy_bitmap(note_bitmap_array[i][j]);
				}
			}
			al_destroy_bitmap(press_key);
			al_destroy_bitmap(line_bitmap);
			al_destroy_bitmap(timer_bitmap);
			al_destroy_bitmap(timer_airplane);
			al_destroy_event_queue(allegro_event_queue);
		}
		
		//이경훈 메모리 해제
		{
			al_destroy_event_queue(event_queue);
			al_destroy_font(font);
			al_destroy_bitmap(title_bar);
			al_destroy_bitmap(track_Rightward);
			al_destroy_bitmap(track_Leftward);
			al_destroy_bitmap(title_Upward);
			al_destroy_bitmap(title_Downward);
			al_destroy_bitmap(start_button);
			al_destroy_bitmap(track_bar);
			closedir(dp); // 디렉토리 해제

			for (i = 0; i < music_number + 2; i++)
			{
				free(title[i]);
			}

			free(title);
		}

		//FMOD 관련 메모리 해제
		{
			g_pSystem->release();
			g_pSystem->close();
		}
	}
	return 0;
}




/*
/* C언어에서 Directory listing을 하는 예제.
/* dirent.h는 윈도우 visualstudio에서 지원이 끊겼기에 git에 올라와있는 것을 사용.
/* git주소는 참조.txt를 참조.
int main(void)
{
	DIR *dp;
	struct dirent *ep;
	dp = opendir("./");

	if (dp != NULL)
	{
		while (ep = readdir(dp))
			puts(ep->d_name);

		(void)closedir(dp);
	}
	else
		perror("Couldn't open the directory");
	
	getchar();

	return 0;
}
*/

void CAT_FILE_STR_BY_NOTE_COLOR(char str[], int x, int y)
{
	memset(str, 0, sizeof(char) * 50);
	strcpy(str, "../GAME_IMAGE/");

	switch (x)
	{
	case 0:
		strncat(str, "Red/", strlen("Red/"));
		break;
	case 1:
		strcat(str, "Orange/");
		break;
	case 2:
		strcat(str, "Yellow/");
		break;
	case 3:
		strcat(str, "Green/");
		break;
	case 4:
		strcat(str, "Blue/");
		break;
	case 5:
		strcat(str, "Indigo/");
		break;
	case 6:
		strcat(str, "Violet/");
	}
	
	switch (y)
	{
	case 0:
		strcat(str, "note");
		break;
	case 1:
		strcat(str, "long_note_up");
		break;
	case 2:
		strcat(str, "long_note_center");
		break;
	case 3:
		strcat(str, "long_note_down");
	}
	strcat(str, ".png");
}


/*
int main()
{
	int title_index = 0; // 곡순서 저장
	char track_num_index = 0; // 트랙번호 배열 index 
	char track_num_str[10][3];//
	char track_num_str[4];
	char music_number = 0; // 음악 개수
	char *picture_name;
	char** title;
	//TESTLINE
	int track_ = 5; //트랙 갯수 임시 변수

	//file directory variable
	DIR *dp;
	struct dirent *dir;
	uint64_t max_file_strlen = 0;

	//key input variable
	bool up = false; // 방향키 위키 아래키 눌렸는지 구분
	bool down = false;
	bool right = false;
	bool left = false;

	//draw control variable
	bool done = false;
	bool draw = true;

	//Allegro variable
	ALLEGRO_EVENT event;
	ALLEGRO_FONT *font = NULL;
	ALLEGRO_BITMAP *title_bar = NULL;
	ALLEGRO_BITMAP *title_Upward = NULL;
	ALLEGRO_BITMAP *title_Downward = NULL;
	ALLEGRO_BITMAP *track_Rightward = NULL;
	ALLEGRO_BITMAP *track_Leftward = NULL;
	ALLEGRO_BITMAP *album_picture = NULL;//앨범 사진
	ALLEGRO_BITMAP *start_button = NULL;
	ALLEGRO_BITMAP *track_bar = NULL;
	ALLEGRO_EVENT_QUEUE *event_queue = al_create_event_queue();

	//알레그로 , 디스플레이 초기화

	// 초기화
	
	

	

	
	font = al_load_font("NanumGothic.ttf", 20, NULL);
	title_bar = al_load_bitmap("Title_bar.png");
	title_Upward = al_load_bitmap("title_up_arrow.png");
	title_Downward = al_load_bitmap("title_down_arrow.png");
	track_Rightward = al_load_bitmap("track_right_arrow.png");
	track_Leftward = al_load_bitmap("track_left_arrow.png");
	album_picture = NULL;//앨범 사진
	start_button = al_load_bitmap("Start_Button.png");
	track_bar = al_load_bitmap("track_bar.png");
	
	// 이벤트 큐 등록
	al_register_event_source(event_queue, al_get_display_event_source(display));
	al_register_event_source(event_queue, al_get_mouse_event_source());
	al_register_event_source(event_queue, al_get_keyboard_event_source());


	if ((dp = opendir("..\\MIDI\\")) == NULL)
	{
		printf("error");
	}

	while ((dir = readdir(dp)) != NULL)
	{
		if ((strstr(dir->d_name, ".mid")) != 0)
		{
			if (strlen(dir->d_name) > max_file_strlen)
			{
				max_file_strlen = dir->d_namlen;
			}
			music_number = music_number + 1;
		}
	}

	closedir(dp);

	//memory allocation layer
	title = (char**)malloc(sizeof(char*) * (music_number + 2));
	for (i = 0; i < music_number + 2; i++)
	{
		title[i] = (char*)malloc(sizeof(char) * (max_file_strlen + 1));
		memset(title[i], 0, sizeof(char) * (max_file_strlen + 1));
	}
	picture_name = (char*)malloc(sizeof(char)*(max_file_strlen + 1 + strlen("..\\Album_Cover\\") + strlen(".jpg")));


	///testing
	if ((dp = opendir("..\\MIDI\\")) == NULL)
	{
		printf("error");
	}

	{
		strcpy(title[0], "- end -");
		int cnt = 0;
		while ((dir = readdir(dp)) != NULL)
		{
			if ((strstr(dir->d_name, ".mid")) != 0)
			{
				strncpy(title[cnt + 1], dir->d_name, strlen(dir->d_name) - 4);
				cnt++;
			}
		}
		strcpy(title[music_number + 1], "- end -");
	}

	for (i = 0; i < music_number + 2; i++)
	{
		printf("DEBUG : %s\n", title[i]);
	}


	// 이벤트 타입
	while (!done)
	{
		al_wait_for_event(event_queue, &event);
		if (event.type == ALLEGRO_EVENT_KEY_DOWN)
		{
			draw = true;
			switch (event.keyboard.keycode)
			{
			case ALLEGRO_KEY_UP: // 곡 위로  up이나 down 이 트루면 music title index가 변함
				up = true;
				break;
			case ALLEGRO_KEY_DOWN: // 곡아래
				down = true;
				break;
			case ALLEGRO_KEY_RIGHT:// 트랙 오른쪽 right 나 left 가 true 면 track_num_str 의 track_num_index가 변함
				right = true;
				break;
			case ALLEGRO_KEY_LEFT://트랙 왼쪽
				left = true;
				break;
			case ALLEGRO_KEY_ESCAPE:
				done = true;
				break;
			case ALLEGRO_KEY_ENTER: // start
				done = true;
				break;
			}
		}
		else if (event.type == ALLEGRO_EVENT_KEY_UP)
		{
			switch (event.keyboard.keycode)
			{

			case ALLEGRO_KEY_ESCAPE:
				done = true;
				break;
			}
		}

		else if (event.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) // 마우스 클릭
		{

			if (event.mouse.button == 1)
			{
				draw = true;

				if ((230 <= event.mouse.x&&event.mouse.x <= 280) && (635 <= event.mouse.y&&event.mouse.y <= 705)) // downward click
				{
					down = true;
				}
				else if ((230 <= event.mouse.x&&event.mouse.x <= 280) && (385 <= event.mouse.y&&event.mouse.y <= 455))// upward click
				{
					up = true;
				}
				else if ((42 <= event.mouse.x&&event.mouse.x <= 92) && (735 <= event.mouse.y&&event.mouse.y <= 770))//left click
				{
					left = true;
				}
				else if ((219 <= event.mouse.x&&event.mouse.x <= 269) && (735 <= event.mouse.y&&event.mouse.y <= 770))//right click
				{
					right = true;
				}
				else if ((297 <= event.mouse.x&&event.mouse.x <= 430) && (744 <= event.mouse.y&&event.mouse.y <= 811)) // start click
				{
					done = true;
				}
			}
		}

		if (draw)
		{
			draw = false;

			// 곡목록 조정
			if (up)
			{
				up = false;
				if (title_index != 0)//밑에 더 없을때 고정
				{
					track_num_index = 0;
					title_index = title_index - 1;
				}
			}
			else if (down)
			{
				down = false;
				if ((title_index + 2) != music_number + 1)
				{
					track_num_index = 0;
					title_index = title_index + 1;
				}
			}


			
			//char track_num_str[3];
			//for (int i = 0; i < 3; i++)
			//{
			//itoa(i + 1, track_num_str, 10);
			//al_draw_text(font, al_map_rgb(0, 0, 0), 112 + (i * 35), 746, ALLEGRO_ALIGN_LEFT, track_num_str);
			//}
			//}
			//
			al_draw_bitmap(track_bar, 102, 686, NULL); // 트랙 숫자보다 위에 있어야 글자가 출력됨
													   //리스트 가운데 있는 노래이름 받아오기(앨범 사진 출력할때 필요)
			strcpy(picture_name, "..\\Album_Cover\\");
			strcat(picture_name, title[title_index + 1]);
			strcat(picture_name, ".png");//확장명 추가
										 //여기서 mtrk_cnt를 읽음!
										 //파일명은 picture_name.mid로함.
										 //mthd_data.mtrk_cnt;


			for (i = 0; i < track_; i++) // 임시로 track_을  track 의 개수로 사용함
			{
				itoa(i + 1, track_num_str[i + 1], 10); // track_num_str[1] ="1" 
			}
			strcpy(track_num_str[0], "-"); //서로 양쪽 끝"-"
			strcpy(track_num_str[track_ + 1], "-");

			if (right == true)
			{
				right = false;
				if (track_num_index + 2 != track_ + 1)
					track_num_index = track_num_index + 1;
			}
			else if (left == true)
			{
				left = false;
				if (track_num_index != 0)
					track_num_index = track_num_index - 1;
			}
			//트랙 넘버 출력
			al_draw_text(font, al_map_rgb(0, 0, 0), 112 + (0 * 35), 746, ALLEGRO_ALIGN_LEFT, track_num_str[track_num_index]);
			al_draw_text(font, al_map_rgb(0, 0, 0), 112 + (1 * 35), 746, ALLEGRO_ALIGN_LEFT, track_num_str[track_num_index + 1]); // 가운데에 있는 번호
			al_draw_text(font, al_map_rgb(0, 0, 0), 112 + (2 * 35), 746, ALLEGRO_ALIGN_LEFT, track_num_str[track_num_index + 2]);

			if ((album_picture = al_load_bitmap(picture_name)) != NULL)
			{
				al_draw_scaled_bitmap(album_picture, 0, 0, al_get_bitmap_width(album_picture), al_get_bitmap_height(album_picture), 100, 50, 310, 310, NULL);

			}
			else//사진 불러오는데 실패 했을때 (존재 하지 않을때?)
			{
				album_picture = al_load_bitmap("NotAlbum.png");
				al_draw_scaled_bitmap(album_picture, 0, 0, al_get_bitmap_width(album_picture), al_get_bitmap_height(album_picture), 100, 50, 310, 310, NULL);

			}


			//al_draw_scaled_bitmap(album_picture[i], 0, 0, al_get_bitmap_width(album_picture[i]), al_get_bitmap_height(album_picture[i]), 0, 0, 200, 200, NULL);
			//delta 만큼 색깔 변화
			//al_draw_scaled_bitmap(Upward, 0, 0, al_get_bitmap_width(Upward), al_get_bitmap_height(Upward), 138, 644, 35, 49, NULL);
			//al_draw_scaled_bitmap(Downward, 0, 0, al_get_bitmap_width(Downward), al_get_bitmap_height(Downward), 138, 819, 35, 49, NULL);
			al_draw_bitmap(title_bar, 55, 470, NULL);
			//al_draw_bitmap(track_bar, 85, 732, NULL);
			al_draw_bitmap(track_Leftward, 43, 739, NULL);
			al_draw_bitmap(track_Rightward, 218, 739, NULL);
			al_draw_bitmap(title_Upward, 230, 385, NULL);
			al_draw_bitmap(title_Downward, 230, 635, NULL);
			al_draw_bitmap(start_button, 295, 745, NULL);
			al_draw_text(font, al_map_rgb(0, 0, 0), 55, 490, ALLEGRO_ALIGN_LEFT, title[title_index]);
			al_draw_text(font, al_map_rgb(0, 0, 0), 55, 540, ALLEGRO_ALIGN_LEFT, title[title_index + 1]);
			al_draw_text(font, al_map_rgb(0, 0, 0), 55, 590, ALLEGRO_ALIGN_LEFT, title[title_index + 2]);
			al_flip_display();
			al_clear_to_color(al_map_rgb(0, 0, 0));
			al_destroy_bitmap(album_picture);
		}


	}

	//track_number = (variable_name)
	//					atoi(track_num_str[track_num_index+1]);
	//album_str = (Variable_name)
	//					title[title_index+1];

	//tilte이 뭐가 들어갈지
	//track num에는 어떤 변수가 들어갈지

	// 메모리 해제부분 
	al_destroy_event_queue(event_queue);
	al_destroy_display(display);
	al_destroy_font(font);
	al_destroy_bitmap(title_bar);
	al_destroy_bitmap(track_Rightward);
	al_destroy_bitmap(track_Leftward);
	al_destroy_bitmap(title_Upward);
	al_destroy_bitmap(title_Downward);
	al_destroy_bitmap(start_button);
	//al_destroy_bitmap(start_button_on);
	al_destroy_bitmap(track_bar);
	closedir(dp); // 디렉토리 해제

				  //memory free layer
	for (i = 0; i < music_number + 2; i++)
	{
		free(title[i]);
	}

	free(title);
	return 0;
}
*/