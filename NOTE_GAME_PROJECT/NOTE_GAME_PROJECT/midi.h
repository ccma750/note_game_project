#ifndef _MIDI_H_
#define _MIDI_H_
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <inttypes.h>
#include <time.h>

//FMOD 헤더 선언
#include "../inc/fmod.hpp"
//FMOD lib 추가
#pragma comment(lib, "../lib/fmodex_vc.lib")

#define READ_MIDI(fp) f_ch = fgetc(fp)
#define FMOD_MAX_CHANNELS 1

struct MTHD_DATA {
	short format;
	short mtrk_cnt;
	unsigned int ticks_per_quater_note;//4분음표당 몇틱이 필요한가
};

struct MTRK_DATA {
	uint64_t delta_time;
	char data1, h_status, l_status;
	short status;
	uint64_t v_length;
};

struct MIDI_SETTING {
	int BPM = 120;//반올림
	int TimeSignatureNumerator = 4;//분자, Default = 4
	int TimeSignatureDenominator = 4;//분모, Default = 4;
	double SecondsPerTick;//몇 틱이 모여야 1초가 되는지.
	int EndFlag = 0;//모든 트랙이 FF 2F 00을 만날 경우 트랙의 개수만큼 값을 갖게된다.
	int left_mtrk_cnt;//재생이 필요한 트랙의 개수.
};

//FMOD의 사운드 종류를 설정할때 쓰는 예제.
//바꿀 예정이기도 하다.
enum SOUNDKIND
{
	SELECTED_MUSIC = 0,
	SD_B,
	SD_C,
	SD_D,
	SD_E,
	SD_F,
	SD_G,
	SD_H,
	SD_END
};

//void MIDI(char file_name[]);
void MTHD_FUNCTION(FILE* FD_midi, MTHD_DATA* mthd_data);
void MTRK_FUNCTION(FILE* FD_midi, MTRK_DATA* mtrk_data, double* timer, char* blocks, MIDI_SETTING* midi_setting, MTHD_DATA* mthd_data);
uint64_t GET_DATA_SIZE(FILE* FD_midi);
uint64_t GET_FILE_SIZE(FILE* FD_midi);
uint64_t GET_V_LENGTH(FILE* FD_midi);
void FIND_NEXT_MTRK(FILE* FD_midi);
void READ_DELTA_TIME(FILE* FD_midi, double* timer, MIDI_SETTING* midi_setting);
void READ_MUSIC_TIME(FILE* FD_midi, MTRK_DATA* mtrk_data, double* timer, MIDI_SETTING* midi_setting, MTHD_DATA* mthd_data);
void CLEAR_MEMORY_MIDI_SETTING(MIDI_SETTING *midi_setting);

#endif