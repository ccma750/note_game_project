#include<allegro5\allegro5.h> 
#include<allegro5\allegro_native_dialog.h> 
#include<allegro5\allegro_image.h>
#include<allegro5\allegro_font.h>
#include<allegro5\allegro_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dirent.h"

#define ScreenWidth 510
#define ScreenHeight 880

int main()
{
	int i = 0, j = 0;
	int x = 10, y = 200;
	int delta = 0;
	int title_list=0; // 곡순서 저장
	char music_number = 0; // 음악 개수
	char *picture_name;
	char** title;
	const float FPS = 60.0;
	DIR *dp;
	struct dirent *dir;
	uint64_t max_file_strlen = 0;
	bool up; // 방향키 위키 아래키 눌렸는지 구분
	bool done = false;
	bool draw = true;
	ALLEGRO_DISPLAY *display = NULL;
	ALLEGRO_EVENT event;
	ALLEGRO_FONT *font = NULL;
	ALLEGRO_BITMAP *title_bar = NULL;
	ALLEGRO_BITMAP *title_Upward = NULL;
	ALLEGRO_BITMAP *title_Downward = NULL;
	ALLEGRO_BITMAP *track_Rightward = NULL;
	ALLEGRO_BITMAP *track_Leftward = NULL;
	ALLEGRO_BITMAP *album_picture = NULL;//앨범 사진
	ALLEGRO_BITMAP *start_button = NULL;
	ALLEGRO_BITMAP *track_bar = NULL;
	ALLEGRO_TIMER *timer = NULL;

	//알레그로 , 디스플레이 초기화
	if (!al_init())
	{
		al_show_native_message_box(NULL,
			"Error",
			NULL,
			"Could not Initialize Allegro",
			NULL,
			NULL);
		return -1;
	}

	al_set_new_display_flags(ALLEGRO_WINDOWED);
	display = al_create_display(ScreenWidth, ScreenHeight);
	al_set_window_position(display, 200, 100);
	al_set_window_title(display, "midi_game");

	if (!display)
	{
		al_show_native_message_box(NULL,
			"Error",
			NULL,
			"Could not create Allegro Display",
			NULL,
			NULL);
		return -1;
	}
	
	// 초기화
	al_install_mouse();
	al_install_keyboard();
	al_init_image_addon();
	al_init_font_addon();
	al_init_ttf_addon();

	timer = al_create_timer(1.0 / FPS);
	font = al_load_font("NanumGothic.ttf", 20, NULL);
	title_bar = al_load_bitmap("Title_bar.png");
	title_Upward = al_load_bitmap("title_up_arrow.png");
	title_Downward = al_load_bitmap("title_down_arrow.png");
	track_Rightward = al_load_bitmap("track_right_arrow.png");
	track_Leftward = al_load_bitmap("track_left_arrow.png");
	album_picture = NULL;//앨범 사진
	start_button = al_load_bitmap("Start_Button.png");
	track_bar = al_load_bitmap("track_bar.png");
	ALLEGRO_EVENT_QUEUE *event_queue = al_create_event_queue();
	// 이벤트 큐 등록
	al_register_event_source(event_queue, al_get_display_event_source(display));
	al_register_event_source(event_queue, al_get_mouse_event_source());
	al_register_event_source(event_queue, al_get_keyboard_event_source());
	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	if ((dp = opendir("..\\MIDI\\")) == NULL)
	{
		printf("error");
	}

	while ((dir = readdir(dp)) != NULL)
	{
		if ((strstr(dir->d_name, ".mid")) != 0)
		{
			if (strlen(dir->d_name) > max_file_strlen)
			{
				max_file_strlen = dir->d_namlen;
			}
			music_number = music_number + 1;
		}
	}

	closedir(dp);

	//memory allocation layer
	title = (char**)malloc(sizeof(char*) * (music_number + 2));
	for (i = 0; i < music_number + 2; i++)
	{
		title[i] = (char*)malloc(sizeof(char) * (max_file_strlen + 1));
		memset(title[i], 0, sizeof(char) * (max_file_strlen + 1));
	}
	picture_name = (char*)malloc(sizeof(char)*(max_file_strlen + 1 + strlen("..\\Album_Cover\\") + strlen(".jpg")));


	///testing
	if ((dp = opendir("..\\MIDI\\")) == NULL)
	{
		printf("error");
	}

	{
		strcpy(title[0], "- end -");
		int cnt = 0;
		while ((dir = readdir(dp)) != NULL)
		{
			if ((strstr(dir->d_name, ".mid")) != 0)
			{
				strncpy(title[cnt + 1], dir->d_name, strlen(dir->d_name) - 4);
				cnt++;
			}
		}
		strcpy(title[music_number + 1], "- end -");
	}

	for (i = 0; i < music_number + 2; i++)
	{
		printf("DEBUG : %s\n", title[i]);
	}
	
		  
	// 이벤트 타입
	while (!done)
	{
		al_wait_for_event(event_queue, &event);
		if (event.type == ALLEGRO_EVENT_KEY_DOWN)
		{
			switch (event.keyboard.keycode)
			{
			case ALLEGRO_KEY_W:
				//al_start_timer(timer);
				up = true;
				draw = true;
				break;
			case ALLEGRO_KEY_S:
				//al_start_timer(timer);
				up = false;
				draw = true;
				break;
			case ALLEGRO_KEY_ESCAPE:
				done = true;
				break;
			}
		}
		else if (event.type == ALLEGRO_EVENT_KEY_UP)
		{
			switch (event.keyboard.keycode)
			{

			case ALLEGRO_KEY_ESCAPE:
				done = true;
				break;
			}
		}
		else if (event.type == ALLEGRO_EVENT_TIMER)
		{
			delta = 5 * al_get_timer_count(timer);
			draw = true;
		}
		else if (event.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
		{
			if (event.mouse.button == 1)
			{
				if ((230 <= event.mouse.x&&event.mouse.x <= 280) && (635 <= event.mouse.y&&event.mouse.y <= 705))
				{
					//al_start_timer(timer);
					up = false;
					draw = true;
				}
				if ((230 <= event.mouse.x&&event.mouse.x <= 280) && (385 <= event.mouse.y&&event.mouse.y <= 455))
				{
					//al_start_timer(timer);
					up = true;
					draw = true;
				}
			}
		}
		// 출력
		if (draw)
		{
			if (delta > 240)
			{
				delta = 0;
				al_set_timer_count(timer, 0);
				al_stop_timer(timer);
				if (up == true)
				{
					if (title_list != 0)//밑에 더 없을때 고정
						title_list = title_list - 1;
				}
				else if (up == false)
				{
					if ((title_list + 2) != music_number + 1)
						title_list = title_list + 1;
				}
			}
			al_draw_bitmap(title_bar, 55, 470, NULL);
			//al_draw_bitmap(track_bar, 85, 732, NULL);
			al_draw_bitmap(track_bar, 102, 686, NULL);
			{
				char track_num_str[3];
				for (int i = 0; i < 3; i++)
				{
					itoa(i + 1, track_num_str, 10);
					al_draw_text(font, al_map_rgb(0, 0, 0), 112 + (i * 35), 746, ALLEGRO_ALIGN_LEFT, track_num_str);
				}
			}
			//al_draw_scaled_bitmap(Upward, 0, 0, al_get_bitmap_width(Upward), al_get_bitmap_height(Upward), 138, 644, 35, 49, NULL);
			//al_draw_scaled_bitmap(Downward, 0, 0, al_get_bitmap_width(Downward), al_get_bitmap_height(Downward), 138, 819, 35, 49, NULL);
			al_draw_bitmap(track_Leftward, 43, 739, NULL);
			al_draw_bitmap(track_Rightward, 218, 739, NULL);
			al_draw_bitmap(title_Upward, 230, 385, NULL);
			al_draw_bitmap(title_Downward, 230, 635, NULL);
			al_draw_bitmap(start_button, 295, 745, NULL);
			//리스트 가운데 있는 노래이름 받아오기(앨범 사진 출력할때 필요)
			strcpy(picture_name, "..\\Album_Cover\\");
			strcat(picture_name, title[title_list + 1]);
			strcat(picture_name, ".png");//확장명 추가

			if ((album_picture = al_load_bitmap(picture_name)) != NULL)
			{
				al_draw_scaled_bitmap(album_picture, 0, 0, al_get_bitmap_width(album_picture), al_get_bitmap_height(album_picture), 100, 50, 310, 310, NULL);
			}
			else//사진 불러오는데 실패 했을때 (존재 하지 않을때?)
			{
				album_picture = al_load_bitmap("NotAlbum.png");
				al_draw_scaled_bitmap(album_picture, 0, 0, al_get_bitmap_width(album_picture), al_get_bitmap_height(album_picture), 100, 50, 310, 310, NULL);
			}
			//al_draw_scaled_bitmap(album_picture[i], 0, 0, al_get_bitmap_width(album_picture[i]), al_get_bitmap_height(album_picture[i]), 0, 0, 200, 200, NULL);
			//delta 만큼 색깔 변화
			al_draw_text(font, al_map_rgb(0 + delta, 0 + delta, 0 + delta), 55, 490, ALLEGRO_ALIGN_LEFT, title[title_list]);
			al_draw_text(font, al_map_rgb(0 + delta, 0 + delta, 0 + delta), 55, 540, ALLEGRO_ALIGN_LEFT, title[title_list + 1]);
			al_draw_text(font, al_map_rgb(0 + delta, 0 + delta, 0 + delta), 55, 590, ALLEGRO_ALIGN_LEFT, title[title_list + 2]);
			al_flip_display();
			al_clear_to_color(al_map_rgb(0, 0, 0));
			al_destroy_bitmap(album_picture);
		}
	}
	// 메모리 해제부분 
	al_destroy_event_queue(event_queue);
	al_destroy_display(display);
	al_destroy_font(font);
	al_destroy_bitmap(title_bar);
	al_destroy_bitmap(track_Rightward);
	al_destroy_bitmap(track_Leftward);
	al_destroy_bitmap(title_Upward);
	al_destroy_bitmap(title_Downward);
	al_destroy_bitmap(start_button);
	al_destroy_bitmap(track_bar);
	closedir(dp); // 디렉토리 해제

				  //memory free layer
	for (i = 0; i < music_number + 2; i++)
	{
		free(title[i]);
	}
	free(title);
	return 0;
}